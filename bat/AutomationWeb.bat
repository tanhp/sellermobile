@echo off
echo "Starting...!"
cd /d C:\xampp\htdocs\smc\bat

start /min cmd.exe /c mvn -f ..\pom.xml test -DsuiteXmlFiles=testsuites/AllValidation.xml

timeout 15000
cd C:
taskkill /F /IM chromedriver.exe
taskkill /F /IM chrome.exe
taskkill /F /IM geckodriver.exe
taskkill /F /IM firefox.exe
taskkill /F /IM node.exe
:end
exit /B