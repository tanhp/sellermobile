package autotestng;

import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;
import constant.MyConstants;
import logger.MyLogger;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import utils.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AutoTestNG {
    TestNG testng;
    List<String>sheetRefs = new ArrayList<String>();
	List<Integer>rows = new ArrayList<Integer>();
	public static List<String>FSheets = new ArrayList<String>();
	public static List<Integer>FRows = new ArrayList<Integer>();
	public static List<Integer>FCols = new ArrayList<Integer>();
	public static List<String>FUrl = new ArrayList<String>();
	
	
	private void runTestSuite(String xmlFile, int row, String sheetRef) throws InterruptedException {
        testng = new TestNG();
		String testngSuite = System.getProperty("user.dir")+"/testsuites/" + xmlFile;
        Thread.sleep(5000);
        testng.setTestSuites(Arrays.asList(testngSuite));
        testng.run();
        rows.add(row);
        sheetRefs.add(sheetRef);
	}
	
	public void sendReportToSkype(String fileUrl) {
		 Message skypeMessage = Message.create()
                 .with(Text.rich("Seller automation report " + FormatUtils.getToday() + ": " + fileUrl));
         SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
         SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_BICH, skypeMessage, 1);
	}

    @Test
    public void runAutoTestNG() throws InterruptedException {
    	//resetDefaultValue();
    	//runTestSuite("LoginValidation.xml",1,"'Login'!A1");
    	runTestSuite("ChatValidation.xml",2,"'Chat'!A1");
    	//runTestSuite("ShopInfoValidation.xml",10,"'ShopInfo'!A1");
    	/*runTestSuite("AddProduct.xml",3,"'AddNewProduct'!A1");
    	runTestSuite("EditProduct.xml",4,"'EditProduct'!A1");
    	runTestSuite("InStock.xml",5,"'InStock'!A1");
    	runTestSuite("PromotionValidation.xml",7,"'AddPromotion'!A1");
    	runTestSuite("ShippingSupport.xml",8,"'ShippingSupport'!A1");
    	runTestSuite("PrivateOffer.xml",9,"'PrivateOffer'!A1");
    	runTestSuite("OrderCancel.xml",11,"'CanCancel'!A1");
    	//runTestSuite("CarrierValidation.xml",12,"'Carrier'!A1");
    	runTestSuite("Security.xml",14,"'AddNewProduct'!A1");
    	runTestSuite("Firefox.xml",15,"'AddNewProduct'!A1");*/
		
        try {
        	String filePath = MyConstants.OUTPUT_PATH + "FinalResults.xlsx";
        	String logPath = MyConstants.LOG_PATH + "SellerTestLog.log";
        	File file = new File(filePath);
        	MyLogger.info("Start merging excel files...");
        	ExcelMerger.mergeExcelFiles(file);
            HyperlinkWorker.setUp(filePath, "Header");
        	HyperlinkWorker.cellStyleForHyperlink();
        	for(int i = 0; i<sheetRefs.size(); i++) {
        		HyperlinkWorker.setLinkToSheet(rows.get(i), 0, sheetRefs.get(i));
        	}
        	HyperlinkWorker.tearDown(filePath);
        	FSheets = XLSWorker.FSheets;
        	FRows = XLSWorker.FRows;
        	FCols = XLSWorker.FCols;
        	FUrl = XLSWorker.FUrl;
        	MyLogger.info("Setting failed screenshots to testcase...");
        	for(int i = 0; i < FSheets.size(); i++) {
        		HyperlinkWorker.setUp(filePath, FSheets.get(i));
            	HyperlinkWorker.cellStyleForHyperlink();
            	HyperlinkWorker.setUrlLink(FRows.get(i), FCols.get(i), FUrl.get(i));
                HyperlinkWorker.tearDown(filePath);
        	}
        	// Upload excel result file to google drive
        	MyLogger.info("Uploading excel result file to google drive...");
			String fileUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadExcelFile(filePath);
        	GoogleDrive.setExcelPermission();
        	MyLogger.info("Send report result via skype...");
        	sendReportToSkype(fileUrl);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //@Test
    public void runAutoTestNG2(){
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        List<XmlClass> classes1 = new ArrayList<XmlClass>();
        List<XmlClass> classes2 = new ArrayList<XmlClass>();
        List<Class> listenerClasses = new ArrayList<Class>();

        XmlSuite suite1 = new XmlSuite();
        suite1.setName("LoginSuite");
        XmlTest test1 = new XmlTest(suite1);
        test1.setName("ProgramTest");
        XmlClass class1 = new XmlClass("login.LoginFromSeller");
        classes1.add(class1);
        /*XmlClass class2 = new XmlClass("SampleProgram2");
        classes.add(class2);*/
        test1.setXmlClasses(classes1);
        suites.add(suite1);



       /* XmlSuite suite2 = new XmlSuite();
        suite2.setName("ProgramSuite2");
        XmlTest test2 = new XmlTest(suite2);
        test2.setName("ProgramTest2");
        XmlClass class2 = new XmlClass("chat.SellerChat");
        classes2.add(class2);
        test2.setXmlClasses(classes2);
        suites.add(suite2);*/

        // Create TestNg and execute suite
        TestNG tng = new TestNG();
        tng.setXmlSuites(suites);
        tng.setListenerClasses(listenerClasses);
        tng.run();

        try {
            XLSWorker.mergeExcelFiles(new File(MyConstants.OUTPUT_PATH + "FinalResults.xlsx"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
