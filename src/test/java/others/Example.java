package others;

import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;
import constant.MyConstants;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;
import logger.MyLogger;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import utils.EmailSender;
import utils.FormatUtils;
import utils.SkypeSender;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by TanHP on 7/19/2018.
 */
public class Example {
    IOSDriver iosDriver;

    @BeforeTest
    public void launchApp() throws MalformedURLException, InterruptedException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11.2");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 5S");
        caps.setCapability(MobileCapabilityType.UDID,"46898d5e0e69a5f39990bfffcb0f407bf906e063");
        caps.setCapability(IOSMobileCapabilityType.BUNDLE_ID,"com.sendo.sendoshop");
        iosDriver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"),caps);
        iosDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Thread.sleep(3000);
    }

    @Test
    public void testSP(){
        iosDriver.findElementByAccessibilityId("Đồng ý").click();
        iosDriver.findElementByAccessibilityId("Sản phẩm").click();
        iosDriver.findElementByAccessibilityId("+").click();


        swipeToTop();

        // from date to date
        iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Từ ngày']")).click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")).sendKeys("April");
        //iosDriver.findElementByAccessibilityId("Chọn").click();
    }

    @Test
    public void getOS() throws MalformedURLException, InterruptedException {

        //iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Nhập email hoặc số điện thoại']")).sendKeys("tanhopham1990@gmail.com");
        //iosDriver.findElement(By.xpath("//XCUIElementTypeSecureTextField[@value='Nhập mật khẩu']")).sendKeys("19901309");
        //iosDriver.findElementByAccessibilityId("ic_login").click();
        //iosDriver.findElementByAccessibilityId("Đăng nhập").click();
        iosDriver.findElementByAccessibilityId("Đồng ý").click();
        //iosDriver.findElementByAccessibilityId("iconChatRed").click();
        //iosDriver.findElementByAccessibilityId("Đơn hàng").click();
        iosDriver.findElementByAccessibilityId("Sản phẩm").click();
        iosDriver.findElementByAccessibilityId("+").click();
        Thread.sleep(3000);

        // click (+) to add photos
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell")).click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]")).click();
        iosDriver.findElementByAccessibilityId("Select(1)").click();
        iosDriver.findElementByAccessibilityId("Chọn ngành hàng").click();
        iosDriver.findElementByAccessibilityId("Tìm kiếm ngành hàng").sendKeys("Giúp việc nhà");
        iosDriver.findElementByAccessibilityId("Voucher dịch vụ -> Dịch vụ giúp việc -> Giúp việc nhà").click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Tên sản phẩm *']")).sendKeys("Gift voucher 100");
        iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Mã sản phẩm *']")).sendKeys("SKURANDOM");
        // enter product price
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeTextField")).sendKeys("100");

        swipeToTop();

        // enter product promotion price
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeTextField")).sendKeys("50");
        // from date to date
        //iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Từ ngày']")).click();
        //iosDriver.findElementByAccessibilityId("Chọn").click();
        //iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Đến ngày']")).click();
        // enter weight
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[10]/XCUIElementTypeTextField")).sendKeys("50");
        swipeToTop();

        iosDriver.findElementByAccessibilityId("Mô tả *").click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeWebView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextView")).sendKeys("Mo ta san pham phai co it nhat 30 ky tu");
        iosDriver.findElementByAccessibilityId("iconSaveEditProduct").click();

        iosDriver.findElementByAccessibilityId("Thuộc tính *").click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeTextField")).sendKeys("Service pack");
        iosDriver.findElementByAccessibilityId("HCM-Q1").click();
        iosDriver.findElementByAccessibilityId("iconSaveEditProduct").click();

        if(iosDriver.findElementByAccessibilityId("On")!=null)
            iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[15]/XCUIElementTypeSwitch")).click();
        iosDriver.findElementByAccessibilityId("checked").click();
    }

    @Test
    public void testProductList() throws InterruptedException {
        iosDriver.findElementByAccessibilityId("Đồng ý").click();
        //iosDriver.findElementByAccessibilityId("iconChatRed").click();
        //iosDriver.findElementByAccessibilityId("Đơn hàng").click();
        iosDriver.findElementByAccessibilityId("Sản phẩm").click();
        Thread.sleep(3000);
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch"));
        iosDriver.findElement(By.xpath("//XCUIElementTypeTextField[@value='Nhập tên, mã sản phẩm']")).sendKeys("Ab1821cr1");
        MyLogger.info("Press Enter Key on Android");
        iosDriver.getKeyboard().pressKey(Keys.ENTER);
        System.out.println("Product SKU: " + iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText")).getText());
        System.out.println("Product name: " + iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText[2]")).getText());
        System.out.println("Product price: " + iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText[3]")).getText());
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText[2]")).click();
        // click more button
        iosDriver.findElementByAccessibilityId("iconRightBarButton").click();
        iosDriver.findElementByAccessibilityId("Sao chép");
        iosDriver.findElementByAccessibilityId("Xoá sản phẩm").click();
    }

    @Test
    public void testChat(){
        iosDriver.findElementByAccessibilityId("Đồng ý").click();
        iosDriver.findElementByAccessibilityId("iconChatRed").click();
        iosDriver.findElementByAccessibilityId("Tan Ho").click();
        iosDriver.findElementByAccessibilityId("input_chat_field").sendKeys("Hello buyer");
        iosDriver.findElementByAccessibilityId("Gửi").click();

        iosDriver.findElementByAccessibilityId("Hello buyer");
        iosDriver.findElementByAccessibilityId("iconBack").click();

        System.out.println("User name: " + iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText[3]")).getText());
        System.out.println("Short content: " + iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText")).getText());
    }

    @Test
    public void testProfile(){
        iosDriver.findElementByAccessibilityId("Đồng ý").click();
        iosDriver.findElementByAccessibilityId("Tài khoản").click();
        iosDriver.findElementByAccessibilityId("Thông tin shop").click();
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText"));
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField")).sendKeys("Shopbaby47");
        swipeToTop();
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeTextField"));
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[7]/XCUIElementTypeTextField"));
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[8]/XCUIElementTypeTextField")).sendKeys("123456789");
        //iosDriver.findElementByAccessibilityId("Xong").click();
        swipeToTop();
        //iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[10]/XCUIElementTypeTextField")).sendKeys("Khu che xuat Tan Thuan");
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[11]/XCUIElementTypeStaticText")).click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")).sendKeys("Hồ Chí Minh");
        iosDriver.findElementByAccessibilityId("Xong").click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[12]/XCUIElementTypeStaticText")).click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")).sendKeys("Quận 7");
        iosDriver.findElementByAccessibilityId("Xong").click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[13]/XCUIElementTypeStaticText")).click();
        iosDriver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")).sendKeys("Phường Tân Thuận Đông");
        iosDriver.findElementByAccessibilityId("Xong").click();

        iosDriver.findElementByAccessibilityId("checked").click();
    }

    public void swipeToTop(){
        MyLogger.info("Start short swipe to top on iOS device");
        int height = iosDriver.manage().window().getSize().getHeight();
        int centerX = iosDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.25);
        int bottom = (int) (height * 0.5);
        new TouchAction(iosDriver).press(centerX, bottom).moveTo(0, top-bottom).release().perform();
    }

    @Test
    public void webDriverManager(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://google.com");
        driver.close();
    }

    @Test
    public void handleProxy(){
        // Create proxy class object
        Proxy p=new Proxy();
        // Set HTTP Port to 7777
        p.setHttpProxy("localhost:7777");
        // Create desired Capability object
        DesiredCapabilities cap=new DesiredCapabilities();
        // Pass proxy object p
        cap.setCapability(CapabilityType.PROXY, p);
        // Open  firefox browser
        WebDriver driver=new FirefoxDriver(cap);
    }
}
