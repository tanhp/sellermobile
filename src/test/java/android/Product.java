package android;

import operation.BaseTest;
import org.testng.annotations.Test;

public class Product extends BaseTest {
    @Test
    public void C001_AddNewProduct(){
        performTestCaseUsingExcel("AddNewProduct",2,"\\android\\Product.properties");
    }

    @Test
    public void C001_EditProduct(){
        performTestCaseUsingExcel("EditProduct",2,"\\android\\Product.properties");
    }

    @Test
    public void C001_Instock() {
        performTestCaseUsingExcel("InStock", 2, "\\android\\Product.properties");
    }

    @Test
    public void C001_CopyProduct() {
        performTestCaseUsingExcel("CopyProduct", 2, "\\android\\Product.properties");
    }

    @Test
    public void C001_DeleteProduct() {
        performTestCaseUsingExcel("DeleteProduct", 2, "\\android\\Product.properties");
    }
}

