package category;

import api.APIController;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.testng.annotations.Test;
import utils.FormatUtils;
import utils.XLSWorker;

import java.util.ArrayList;
import java.util.List;

public class ProductAPI {

    List<Long> cateIds = new ArrayList<>();

    //String baseApi = "https://sapi.sendo.vn/shop/";
    //String baseApi = "http://partner.seller.test.sendo.vn/shop/";
    String baseApi = "http://api.partner.seller.test.sendo.vn/shop/";

    JSONObject fullResponse = null;
    JSONArray cateList = null;

    XSSFWorkbook workbook;
    XSSFSheet sheet;

    public void getListCateId(){
        try{
            String apiUrl = baseApi + "category";
            String result = APIController.get(apiUrl);
            System.out.println(result);
            if(!result.contains("error")){
                Object obj = JSONValue.parse(result);
                fullResponse = (JSONObject) obj;
                cateList = (JSONArray) fullResponse.get("result");
                for(int i=0; i< cateList.size();i++){
                    JSONObject cate = (JSONObject) cateList.get(i);
                    Long id = (Long) cate.get("id");
                    Long level = (Long) cate.get("level");
                    if(level==4){
                        System.out.println(id);
                        cateIds.add(id);
                    }
                }
            }
        }catch (Exception e){

        }
    }



    public String getParam(Long childCateId){
        String productInfo = "{" +
                "\"Id\" : 0," +
                "\"Name\" : \"Lorem ipsum dolor sit amet\"," +
                "\"StoreSku\" : \"" + "SKU" + FormatUtils.getCurrentDateTime() + "\"," +
                "\"Price\" : 520000.0," +
                "\"UnitId\" : 1," +
                "\"Weight\" : 120.0," +
                "\"StockAvailability\" : false," +
                "\"CategoryId\" : " + childCateId + "," +
                "\"UrlPicture\" : \"https://via.placeholder.com/500.jpg\"," +
                "\"Description\" : \"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"," +
                "\"Quantity\" : 0," +
                "\"productTags\": \"abc,def\"," +
                "\"BrandId\":0," +
                "\"productDate\": \"0001-01-01 00:00:00\"," +
                "\"updatedDate\": \"0001-01-01 00:00:00\"," +
                "\"productStatus\": 3," +
                "\"listPicture\": [" +
                "            \"http://mtest.scdn.vn/img1/2018/11_2/lWP71n.jpg\"," +
                "            \"http://mtest.scdn.vn/img1/2018/11_2/3tfrGc.jpg\"," +
                "            \"http://mtest.scdn.vn/img1/2018/11_2/3tfrGc.jpg\"" +
                "        ]," +
                "\"lengthProduct\": 14," +
                "\"witdhProduct\": 15," +
                "\"heightProduct\": 20,";

        String attributes = "\"AttributeCollections\" : [";
        String apiUrl = baseApi + "category/id/" + childCateId;
        try{
            System.out.println(apiUrl);
            String result = APIController.get(apiUrl);
            if(!result.contains("error")){
                Object obj = JSONValue.parse(result);
                fullResponse = (JSONObject) obj;
                JSONObject cateResult = (JSONObject) fullResponse.get("result");
                String cateName = (String) cateResult.get("name");
                System.out.println(cateName);
                JSONArray attributeList = (JSONArray) cateResult.get("attribute");
                for(int i=0; i<attributeList.size(); i++){
                    attributes = attributes + "{";
                    JSONObject attr = (JSONObject) attributeList.get(i);
                    Long id = (Long) attr.get("id");
                    String controlType = (String) attr.get("controlType");
                    String attrName = (String) attr.get("name");
                    attributes = attributes + "\"Id\" : " + id + "," +
                            "\"ControlType\" : \"" + controlType + "\"," +
                            "\"Name\" : \"" + attrName + "\"," +
                            "\"IsRequired\" : true,";
                    JSONArray attributeValues = (JSONArray) attr.get("attributeValues");
                    for(int j = 0; j <= 0; j++){
                        JSONObject attrValue = (JSONObject) attributeValues.get(j);
                        Long id1 = (Long) attrValue.get("id");
                        String value = (String) attrValue.get("value");
                        if(controlType.equals("TextBox")){
                            value = "test";
                        }
                        attributes = attributes + "\"AttributeValues\" : [{" +
                                "\"Id\" : " + id1 + "," +
                                "\"Value\" : \"" + value + "\"," +
                                "\"IsSelected\" : true" +
                                "}]";
                    }
                    if(i==attributeList.size()-1){
                        attributes = attributes + "}]}";
                    }else{
                        attributes = attributes + "},";
                    }
                }
                XLSWorker.updateExcel(sheet,apiUrl,cateName,attributes);
                return productInfo + attributes;
            }
        }catch (Exception e){
            XLSWorker.updateExcel(sheet,apiUrl,"null","null");
        }
        return null;
    }

    public void addNewProduct(Long cateId){
        String param = getParam(cateId);
        String source = baseApi + "product";
        System.out.println(source);
        try{
            if(param!=null){
                System.out.println(param);
                String result = APIController.post(source,param);
                System.out.println(result);
                if(!result.contains("error")) {
                    Object obj = JSONValue.parse(result);
                    fullResponse = (JSONObject) obj;
                    JSONObject cateResult = (JSONObject) fullResponse.get("result");
                    Boolean status = (Boolean) cateResult.get("status");
                    String productId = (String) cateResult.get("message");
                    XLSWorker.updateExcel(sheet,status,productId);
                }
            }
        }catch (Exception e){

        }
    }

    @Test
    public void generateData() {
        workbook = XLSWorker.createWorkbook();
        sheet = XLSWorker.createSheet(workbook,"Category");
        getListCateId();
        for(int i=0; i<cateIds.size();i++){
            addNewProduct(cateIds.get(i));
        }

        XLSWorker.writeCategoryExcel(workbook);
    }
}
