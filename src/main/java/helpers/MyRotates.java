package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.ScreenOrientation;

public class MyRotates {
    AppiumDriver<MobileElement> driver;
    ScreenOrientation orientation = driver.getOrientation();

    public void setScreenLandscape(){
        driver.rotate(ScreenOrientation.LANDSCAPE);
    }

    public void setScreenPortrait(){
        driver.rotate(ScreenOrientation.PORTRAIT);
    }
}
