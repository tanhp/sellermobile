package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class MyActions {
    private WebDriver driver;
    private MyHelper helper;
    private Actions builder;
    private String exception;

    public MyActions(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
        this.builder = new Actions(this.driver);
    }

    public void dragAndDrop(WebElement elementFrom, WebElement elementTo){
        Action dragAndDrop = builder.clickAndHold(elementFrom)
                .moveToElement(elementTo)
                .release(elementTo)
                .build();
        dragAndDrop.perform();
    }

    public void mouseHover(WebElement element){
    	System.out.println("G");
    	try {
    		builder.moveToElement(element).build().perform();
    	}catch(Exception e) {
    		System.out.println("Exception: " + e);
    	}
        helper.myBasics().sleep(2000);
    	System.out.println("H");
    }

    public void rightClick(WebElement element){
        builder.contextClick(element).build().perform();
    }

    public void doubleClick(WebElement element){
        builder.doubleClick(element).build().perform();
    }

}
