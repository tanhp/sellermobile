package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import classes.MyClasses;
import poms.*;

public class MyHelper {
    private AppiumDriver<MobileElement> driver;

    public MyHelper(AppiumDriver<MobileElement> driver){
        this.driver = driver;
    }

    private MyJavascripts myJavascripts;
    private MyWaits myWaits;
    private MyVerifys myVerifys;
    private MyBasics myBasics;
    private MyActions myActions;
    private MyTools myTools;
    private MySwitchs mySwitchs;
    private MyClasses myObjects;
    private SellerAction sellerAction;
    private MyKeyboards myKeyboards;
    private MySwipes mySwipes;
    private MyWebDriver myWebDriver;

    private Product product;
    private Chat chat;
    private ShopInfo profile;
    private SalesOrder salesOrder;

    public MyJavascripts myJavascripts(){
        if(this.myJavascripts == null)
            myJavascripts = new MyJavascripts(driver,this);
        return myJavascripts;
    }

    public MyWaits myWaits(){
        if(myWaits == null)
            myWaits = new MyWaits(driver,this);
        return myWaits;
    }

    public MyVerifys myVerifys(){
        if(myVerifys == null)
            myVerifys = new MyVerifys(driver, this);
        return myVerifys;
    }

    public MyBasics myBasics(){
        if(myBasics == null)
            myBasics = new MyBasics(driver, this);
        return myBasics;
    }

    public MyActions myActions(){
        if(myActions == null)
            myActions = new MyActions(driver, this);
        return myActions;
    }

    public MyKeyboards myKeyboards(){
        if(myKeyboards == null)
            myKeyboards = new MyKeyboards(driver);
        return myKeyboards;
    }

    public MyTools myTools(){
        if(myTools == null)
            myTools = new MyTools(driver, this);
        return myTools;
    }

    public MySwitchs mySwitchs(){
        if(mySwitchs == null)
            mySwitchs = new MySwitchs(driver, this);
        return mySwitchs;
    }

    public MySwipes mySwipes(){
        if(mySwipes == null)
            mySwipes = new MySwipes(driver,this);
        return mySwipes;
    }

    public MyWebDriver myWebDriver(){
        if(myWebDriver == null)
            myWebDriver = new MyWebDriver(this);
        return myWebDriver;
    }

    public MyClasses myObjects(){
        if(myObjects == null)
            myObjects = new MyClasses();
        return myObjects;
    }

    public SellerAction sellerAction(){
        if(sellerAction == null)
            sellerAction = new SellerAction(this);
        return sellerAction;
    }

    public Product product(){
        if(product == null)
            product = new Product(this);
        return product;
    }

    public Chat chat(){
        if(chat == null)
            chat = new Chat(this);
        return chat;
    }

    public ShopInfo profile(){
        if(profile == null)
            profile = new ShopInfo(this);
        return profile;
    }

    public SalesOrder salesOrder(){
        if(salesOrder == null)
            salesOrder = new SalesOrder(this);
        return salesOrder;
    }

}



