package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.TestException;

import java.util.List;

public class MyJavascripts {

    WebDriver driver;
    JavascriptExecutor js;
    MyHelper helper;
    private Actions actions;

    MyJavascripts(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.js = (JavascriptExecutor) driver;
        this.helper = helper;
    }

    public WebElement getElementByIdByJS(String id){
        WebElement element;
        element =  (WebElement) js.executeScript("return document.getElementById('"+ id + "')");
        return element;
    }

    public List<WebElement> getElementsByClassNameByJS(String className){
        List<WebElement> elements;
        elements =  (List<WebElement>) js.executeScript("return document.getElementsByClassName('"+ className + "')");
        return elements;
    }

    public String getTextByJS(String xpath){
        return (String) (js.executeScript("angular.element($x('"+ xpath +"')).text()"));
    }

    public void getCSSValue(By selector){
        WebElement switchLabel = driver.findElement(By.cssSelector(".className"));
        String colorRGB = ((JavascriptExecutor)driver)
                .executeScript("return window.getComputedStyle(arguments[0], ':before').getPropertyValue('background-color');",switchLabel).toString();
    }

    public void clickByJS(WebElement element){
        // Solution for the element is not clickable.
        try {
            js.executeScript("arguments[0].click();", element);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void typeByJS(WebElement element, String value){
        // Solution for invalid element state.
        js.executeScript("arguments[0].value='"+ value +"'", element);
    }

    public void scrollToEndPage(){
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollToElement(WebElement element){
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void scrollToThenClick(By selector) {
        WebElement element = helper.myBasics().getElement(selector);
        actions = new Actions(driver);
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            actions.moveToElement(element).perform();
            actions.click(element).perform();
            helper.myWaits().waitForPageLoad();
        } catch (Exception e) {
            System.out.println("Failed to click element: " + e.getMessage());
            throw new TestException(String.format("The following element is not clickable: [%s]", element.toString()));
        }
    }

    public void scrollDownPage(String value){
        js.executeScript("window.scrollBy(0,"+ value +")");
    }

    public void highlightElement(WebElement element) {
        js.executeScript("arguments[0]" + ".setAttribute('style','background-color: yellow; border: 2px solid red;')", element);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        js.executeScript("arguments[0]" + ".setAttribute('style','border: 2px solid white;')", element);
    }

    public void innerHtmlByJs(String xpath, String value){
        js.executeScript("document.evaluate('"+ xpath +"',document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)" +
                ".singleNodeValue.innerHTML = '"+ value +"';");
    }

    public void highlightElement(WebDriver driver, WebElement element) {

        JavascriptExecutor js = ((JavascriptExecutor) driver);

        js.executeScript("arguments[0]" + ".setAttribute('style','background-color: yellow; border: 2px solid red;')",
                element);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        js.executeScript("arguments[0]" + ".setAttribute('style','border: 2px solid white;')", element);

    }
}
