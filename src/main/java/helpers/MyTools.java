package helpers;

import constant.MyConstants;
import logger.MyLogger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import utils.FormatUtils;
import utils.GoogleDrive;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static operation.ParallelTest.exception;
import static org.testng.Assert.assertTrue;


public class MyTools {
    WebDriver driver;
    MyHelper helper;
    private String imageFilePath;
    private String imageUrl;

    public MyTools(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public String getImageFilePath(){
        return imageFilePath;
    }

    public String getImageUrl(){
        return imageUrl;
    }

    public String takeScreenshot(String method) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String screenshotName = dtf.format(now) + "-" + method + "-" + FormatUtils.GenerateNumber(10000) + ".jpg";
            imageFilePath = MyConstants.SCREENSHOT_PATH + screenshotName;
            //imageUrl = MyConstants.SCREENSHOT_URL + screenshotName;
            FileUtils.copyFile(scrFile, new File(imageFilePath));
            imageUrl = "https://drive.google.com/open?id=" + uploadPhotoToDrive(imageFilePath);
            MyLogger.info("Image url on google drive is: " + imageUrl);
        } catch (Exception e) {
        	logFail("Have an error occurring while taking failed testcase photo!");
            return null;
        }
        return imageFilePath;
    }
    
    public String uploadPhotoToDrive(String filePath) {
    	String screenshotUrl = "";
    	try {
    		if(filePath==null)
        		return "Failed to capture a photo because have an error occurs while capturing!";
    		//Upload screenshot to google drive
            screenshotUrl = GoogleDrive.uploadScreenshot(filePath);
            if(screenshotUrl == null)
            	return "Have an error occurring while uploading photo to google drive";
            GoogleDrive.setPhotoPermission();
    	}catch(Exception e) {
    		return "Undentified error!";
    	}
    	return screenshotUrl;
    }

    public void uploadByRobot(String image){
        StringSelection ss = new StringSelection(MyConstants.IMAGES_PATH + image);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        helper.myBasics().sleep(1000);
        try {
            Robot robot = new Robot();
            if(System.getProperty("os.name").startsWith("Windows")){
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }else if(System.getProperty("os.name").startsWith("Mac")){
                // Cmd + Tab is needed since it launches a Java app and the browser looses focus
                robot.keyPress(KeyEvent.VK_META);
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_META);
                robot.keyRelease(KeyEvent.VK_TAB);
                robot.delay(500);
                //Open Goto window
                robot.keyPress(KeyEvent.VK_META);
                robot.keyPress(KeyEvent.VK_SHIFT);
                robot.keyPress(KeyEvent.VK_G);
                robot.keyRelease(KeyEvent.VK_META);
                robot.keyRelease(KeyEvent.VK_SHIFT);
                robot.keyRelease(KeyEvent.VK_G);
                //Paste the clipboard value
                robot.keyPress(KeyEvent.VK_META);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_META);
                robot.keyRelease(KeyEvent.VK_V);
                //Press Enter key to close the Goto window and Upload windo
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
                robot.delay(500);
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        helper.myBasics().sleep(1000);
    }

    public static String CaptureScreenShotByRobot(String testStepsName) {
        try{
            Robot robotClassObject = new Robot();
            Rectangle screenSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage tmp = robotClassObject.createScreenCapture(screenSize);
            String path=System.getProperty("user.dir")+"/ScreenCapturesPNG/"+testStepsName+System.currentTimeMillis()+".png";
            ImageIO.write(tmp, "png",new File(path));
            return path;
        }catch(Exception e) {
            System.out.println("Some exception occured." + e.getMessage());
            return "";
        }
    }

    public void writeJSONFile(JSONObject obj, String fileName) {
        //File file = new File(System.getProperty("user.dir") + "//data//" + fileName);
        File file = new File(MyConstants.OUTPUT_PATH + fileName);
        PrintWriter out = null;
        String content = null;
        try {
            out = new PrintWriter(new FileWriter(file, true));
            String item = obj.toString().replace("[", "").replace("]", "");
            out.append(item);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
            try {
                content = IOUtils.toString(new FileInputStream(file), "UTF-8");
                if(content.contains("}{")) {
                    content = content.replace("}{", ",");
                }
                IOUtils.write(content, new FileOutputStream(file), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void logFail(String log) {
    	exception = log;
    	MyLogger.warn(log);
    	assertTrue(false);
    }
}
