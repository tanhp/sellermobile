package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.mobile.NetworkConnection;

public class MyConnections {
    AppiumDriver<MobileElement> driver;
    NetworkConnection connection = (NetworkConnection) driver;

    public void setAirplaneMode(){
        if(connection.getNetworkConnection() != NetworkConnection.ConnectionType.AIRPLANE_MODE)
            connection.setNetworkConnection(NetworkConnection.ConnectionType.AIRPLANE_MODE);
    }

    public void setWifi(){
        if(connection.getNetworkConnection() != NetworkConnection.ConnectionType.WIFI)
            connection.setNetworkConnection(NetworkConnection.ConnectionType.WIFI);
    }

    public void setData(){
        if(connection.getNetworkConnection() != NetworkConnection.ConnectionType.DATA)
            connection.setNetworkConnection(NetworkConnection.ConnectionType.DATA);
    }
}
