package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import logger.MyLogger;
import org.openqa.selenium.By;

public class MySwipes {
    AppiumDriver<MobileElement> androidDriver;
    AppiumDriver<MobileElement> iosDriver;
    MyHelper helper;

    public MySwipes(AppiumDriver<MobileElement> driver, MyHelper helper){
        this.androidDriver = driver;
        this.iosDriver = driver;
        this.helper = helper;
    }

    public void swipeToTop(int times) {
        int height = androidDriver.manage().window().getSize().getHeight();
        int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.25);
        int bottom = (int) (height * 0.75);
        for (int i = 0; i<times; i++) {
            new TouchAction(androidDriver).press(centerX, bottom).waitAction().moveTo(centerX, top).release().perform();
        }
    }

    public void shortSwipeToTop(String platform) {
        if(platform.equals("Android")) {
            MyLogger.info("Start short swipe to top on Android device");
            int height = androidDriver.manage().window().getSize().getHeight();
            int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
            int top = (int) (height * 0.25);
            int bottom = (int) (height * 0.5);
            new TouchAction(androidDriver).press(centerX, bottom).waitAction().moveTo(centerX, top).release().perform();
        }else{
            MyLogger.info("Start short swipe to top on iOS device");
            int height = iosDriver.manage().window().getSize().getHeight();
            int centerX = iosDriver.manage().window().getSize().getWidth() / 2;
            int top = (int) (height * 0.25);
            int bottom = (int) (height * 0.5);
            new TouchAction(iosDriver).press(centerX, bottom).moveTo(0, top-bottom).release().perform();
        }
    }

    public void miniSwipeToTop() {
        MyLogger.info("Start mini swipe to top on Android device");
        int height = androidDriver.manage().window().getSize().getHeight();
        int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.25);
        int bottom = (int) (height * 0.35);
        new TouchAction(androidDriver).press(centerX, bottom).waitAction().moveTo(centerX, top).release().perform();
    }

    public void swipeToBot(int times) {
        int height = androidDriver.manage().window().getSize().getHeight();
        int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.25);
        int bottom = (int) (height * 0.75);
        for (int i = 0; i<times; i++) {
            new TouchAction(androidDriver).press(centerX, top).waitAction().moveTo(centerX, bottom).release().perform();
        }
    }

    public void swipeToBot(){
        int height = androidDriver.manage().window().getSize().getHeight();
        int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.01);
        int bottom = (int) (height * 0.75);
        new TouchAction(androidDriver).press(centerX, top).waitAction().moveTo(centerX, bottom).release().perform();
    }

    public void shortSwipeToBot() {
        MyLogger.info("Start short swipe to bot on Android device");
        int height = androidDriver.manage().window().getSize().getHeight();
        int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.25);
        int bottom = (int) (height * 0.5);
        new TouchAction(androidDriver).press(centerX, top).waitAction().moveTo(centerX, bottom).release().perform();
    }

    public void miniSwipeToBot() {
        MyLogger.info("Start mini swipe to bot on Android device");
        int height = androidDriver.manage().window().getSize().getHeight();
        int centerX = androidDriver.manage().window().getSize().getWidth() / 2;
        int top = (int) (height * 0.25);
        int bottom = (int) (height * 0.35);
        new TouchAction(androidDriver).press(centerX, top).waitAction().moveTo(centerX, bottom).release().perform();
    }

    public void pullToRefresh() {
        MyLogger.info("Start pull to refresh on Android device");
        swipeToBot(1);
    }

    public void swipeToLeft(){
        MyLogger.info("Start short swipe to left on Android device");
        int centerY = androidDriver.manage().window().getSize().getHeight()/ 2;
        int width = androidDriver.manage().window().getSize().getWidth();
        int right = (int) (width * 0.8);
        int left = (int) (width * 0.2);
        new TouchAction(androidDriver).press(right, centerY).waitAction().moveTo(left, centerY).release().perform();
    }

    public void swipeToRight(){
        MyLogger.info("Start short swipe to right on Android device");
        int centerY = androidDriver.manage().window().getSize().getHeight()/ 2;
        int width = androidDriver.manage().window().getSize().getWidth();
        int right = (int) (width * 0.8);
        int left = (int) (width * 0.2);
        new TouchAction(androidDriver).press(left, centerY).waitAction().moveTo(right, centerY).release().perform();
    }

    public void swipeElementToLeft(By selector){
        MyLogger.info("Start swipe element to left");
        int Y = androidDriver.findElement(selector).getLocation().getY();
        int width = androidDriver.manage().window().getSize().getWidth();
        int right = (int) (width * 0.8);
        int left = (int) (width * 0.2);
        new TouchAction(androidDriver).press(right, Y).waitAction().moveTo(left, Y).release().perform();
    }

    public void swipeElementToRight(By selector){

    }

    public void swipeElementToTop(By selector){}

    public void swipeElementToBot(By selector){}


}
