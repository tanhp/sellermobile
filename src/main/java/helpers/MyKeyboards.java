package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import logger.MyLogger;

public class MyKeyboards {
    AppiumDriver<MobileElement> driver;

    public MyKeyboards(AppiumDriver<MobileElement> driver){
        this.driver = driver;
    }

    public void pressAndroidKey(String key){
        switch(key){
            case "ENTER":
                MyLogger.info("Press Enter Key on Android");
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.ENTER);
                break;
            case "BACK":
                MyLogger.info("Press back on Android");
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.BACK);
                break;
            case "HOME":
                MyLogger.info("Press home on Android");
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.HOME);
                break;
            case "0":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_0);
                 break;
            case "1":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_1);
                break;
            case "2":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_2);
                break;
            case "3":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_3);
                break;
            case "4":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_4);
                break;
            case "5":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_5);
                break;
            case "6":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_6);
                break;
            case "7":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_7);
                break;
            case "8":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_8);
                break;
            case "9":
                ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.KEYCODE_9);
                break;
        }

    }

}
