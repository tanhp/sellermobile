package helpers;

import constant.MyConstants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.TestException;

import logger.MyLogger;
import report.ExtentReport;
import utils.ExcelMerger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static operation.ParallelTest.exception;
import static org.testng.Assert.assertTrue;

public class MyBasics {
    AppiumDriver<MobileElement> driver;
    AndroidDriver androidDriver;
    MyHelper helper;

    public MyBasics(AppiumDriver<MobileElement> driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void switchToBuyerApp(){
        ((AndroidDriver)driver).startActivity(new Activity(MyConstants.buyerAppPackage, MyConstants.buyerAppActivity));
    }


    public MobileElement getElement(By selector) {
        try {
            helper.myWaits().waitForElementToBeVisible(selector);
            return driver.findElement(selector);
        } catch (Exception e) {
            exception = String.format("Element %s does not exist - proceeding", selector);
            MyLogger.error(exception);
        }
        return null;
    }

    public MobileElement getElementMayNull(By selector){
        try {
            helper.myWaits().waitForElementToBeVisibleMayNull(selector);
            return driver.findElement(selector);
        } catch (Exception e) {
        }
        return null;
    }

    public List<MobileElement> getElements(By Selector) {
        helper.myWaits().waitForElementToBeVisible(Selector);
        try {
            return driver.findElements(Selector);
        } catch (Exception e) {
            exception = String.format("The following element did not display: [%s] ", Selector.toString());
            throw new NoSuchElementException(exception);
        }
    }

    public List<String> getListOfElementTexts(By selector) {
        List<String> elementList = new ArrayList<String>();
        List<MobileElement> elements = getElements(selector);

        for (MobileElement element : elements) {
            if (element == null) {
                throw new TestException("Some elements in the list do not exist");
            }
            if (element.isDisplayed()) {
                elementList.add(element.getText().trim());
            }
        }
        return elementList;
    }

    public MobileElement getElementNotVisible(By selector){
        // Solution for element is not visible
        int elementSize = helper.myBasics().getElements(selector).size();
        return helper.myBasics().getElements(selector).get(elementSize-1);
    }

    public void clickStaleElement(By selector){
        // Solution for stale element
        for(int i=0; i<3; i++){
            try{
                helper.myBasics().getElement(selector).click();
                break;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public void click(By selector) {
        MobileElement element = getElement(selector);
        helper.myWaits().waitForElementToDisplay(selector);
        helper.myWaits().waitForElementToBeClickable(selector);
        try {
            element.click();
        } catch (Exception e) {
            exception = String.format("The following element is not clickable: [%s]", selector);
            MyLogger.error(exception);
            throw new TestException(String.format("The following element is not clickable: [%s]", selector));
        }
    }

    public void click(MobileElement element){
        try {
            element.click();
        } catch (Exception e) {
            exception = String.format("Element is not clickable!");
            MyLogger.error(exception);
        }
    }

    public void clickMayNull(By selector){
    	helper.myBasics().sleep(2000);
        MobileElement element = getElementMayNull(selector);
        if(element == null)
            return;
        try {
            helper.myWaits().waitForElementToDisplay(selector);
            helper.myWaits().waitForElementToBeClickable(selector);
            element.click();
        } catch (Exception e) {
        	MyLogger.error("Error: " + e.getMessage());
        }
    }

    public void sendKeys(By selector, String value) {
        MobileElement element = getElement(selector);
        helper.myWaits().waitForElementToDisplay(selector);
        try {
            element.sendKeys(value);
        } catch (Exception e) {
            exception = String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString());
            MyLogger.error(exception);
            throw new TestException(exception);
        }
    }

    public void sleep(final long millis) {
        System.out.println((String.format("sleeping %d ms", millis)));
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void setClick(By selector){
        sleep(2000);
        helper.myJavascripts().clickByJS(driver.findElement(selector));
    }


    public void setDate(By selector){
        helper.myBasics().click(selector);
        helper.myBasics().click(helper.myBasics().getElements(By.xpath("//android.widget.Button[@index='2']")).get(1));
        helper.myBasics().click(By.id("android:id/button1"));
    }

    public void setSwitch(By selector, String value){
        MobileElement switcher = helper.myBasics().getElement(selector);
        String status = switcher.getText();
        System.out.println(status);
        if(value.equals("ON")){
            if(status.equals("OFF"))
                switcher.click();
        }else{
            if(status.equals("ON"))
                switcher.click();
        }
    }

    public void setComboBox(By selector, String value){
        helper.myBasics().click(selector);
        helper.myBasics().click(By.xpath("//android.widget.TextView[@text='" + value + "']"));
    }

    public String getTextFromInput(By selector){
        return getElement(selector).getText();
    }

    public String getNotificationContent(){
        helper.mySwipes().swipeToBot();
        String bigText = helper.myBasics().getElement(By.id("android:id/big_text")).getText();
        return bigText;
    }

    public void pressNumber(By selector, String digits){
        helper.myBasics().getElement(selector).equals(driver.switchTo().activeElement());
        for(int i=0; i<digits.length(); i++){
            helper.myKeyboards().pressAndroidKey(String.valueOf(digits.charAt(i)));
        }
    }

    public MobileElement getAndroidElementTest(String using){
        using = "new UiSelector().index(0)";
        //return (MobileElement)driver.findElements(MobileBy.AndroidUIAutomator(using));
        return (MobileElement)((AndroidDriver)driver).findElementByAndroidUIAutomator(using);
    }

    public MobileElement getIOSElementTest(String using){
        using = "new UiSelector().text('Menu')";
        //return (MobileElement)driver.findElement(MobileBy.IosUIAutomation(".elements()"));
        return (MobileElement)((IOSDriver)driver).findElementsByIosUIAutomation(using);
    }
}
