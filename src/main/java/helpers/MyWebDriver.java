package helpers;

import constant.MyConstants;
import logger.MyLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyWebDriver {
    WebDriver driver;
    MyHelper helper;
    private WebDriverWait wait;
    private int timeout = 10;

    public MyWebDriver(MyHelper helper) {
        this.helper = helper;
        this.driver = initDriver();
    }

    public WebDriver initDriver() {
        try {
            System.setProperty("webdriver.chrome.driver", MyConstants.getChromeDriverPath());
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-extensions");
            //options.addArguments("--headless");
            options.addArguments("--start-maximized");
            options.addArguments("disable-infobars"); // disabling infobars
            options.addArguments("--disable-extensions"); // disabling extensions
            options.addArguments("--disable-gpu"); // applicable to windows os only
            options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            options.addArguments("--no-sandbox"); // Bypass OS security model
            options.setExperimentalOption("prefs", prefs);
            options.setExperimentalOption("useAutomationExtension", false);
            return new ChromeDriver(options);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
    }

    public void closeDriver() {
        driver.close();
    }

    public void navigateToURL(String URL) {
        if (!MyConstants.BASE_SELLER_URL.equals("https://ban.sendo.vn"))
            URL = URL.replace("https://ban.sendo.vn", MyConstants.BASE_SELLER_URL);
        try {
            driver.navigate().to(URL);
            waitForPageLoad();
        } catch (Exception e) {
            MyLogger.error("FAILURE: URL did not load: " + URL);
        }
    }

    public WebElement getElement(By selector) {
        try {
            waitForElementToBeVisible(selector);
            return driver.findElement(selector);
        } catch (Exception e) {
            MyLogger.error(String.format("Element %s does not exist - proceeding", selector));
        }
        return null;
    }

    public WebElement getElementMayNull(By selector) {
        try {
            waitForElementToBeVisibleMayNull(selector);
            return driver.findElement(selector);
        } catch (Exception e) {
        }
        return null;
    }

    public List<WebElement> getElements(By Selector) {
        waitForElementToBeVisible(Selector);
        try {
            return driver.findElements(Selector);
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element did not display: [%s] ", Selector.toString()));
        }
    }

    public void click(By selector) {
        WebElement element = getElement(selector);
        waitForElementToDisplay(selector);
        waitForElementToBeClickable(selector);
        try {
            element.click();
        } catch (Exception e) {
            MyLogger.error(String.format("The following element is not clickable: [%s]", selector));
        }
    }

    public void clickMayNull(By selector) {
        sleep(2000);
        WebElement element = getElementMayNull(selector);
        if (element == null)
            return;
        try {
            waitForElementToDisplay(selector);
            waitForElementToBeClickable(selector);
            element.click();
        } catch (Exception e) {
            MyLogger.error("Error: " + e.getMessage());
        }
    }

    public void sendKeys(By selector, String value) {
        WebElement element = getElement(selector);
        waitForElementToDisplay(selector);
        clearField(element);
        try {
            element.sendKeys(value);
        } catch (Exception e) {
            MyLogger.error(String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString()));
        }
    }

    public void clearField(WebElement element) {
        try {
            element.clear();
            waitForElementTextToBeEmpty(element);
        } catch (Exception e) {
            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
        }
    }

    public void sleep(final long millis) {
        System.out.println((String.format("sleeping %d ms", millis)));
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void switchToNewFrame(WebElement element) {
        driver.switchTo().frame(element);
    }

    public void reloadPage() {
        driver.navigate().refresh();
        waitForPageLoad();
    }

    public void waitForPageLoad() {
        String state = null;
        String oldstate = null;
        try {
            int i = 0;
            while (i < 5) {
                Thread.sleep(1000);
                state = ((JavascriptExecutor) driver).executeScript("return document.readyState;").toString();
                //System.out.print("." + Character.toUpperCase(state.charAt(0)) + ".");
                if (state.equals("interactive") || state.equals("loading"))
                    break;
                /*
                 * If browser in 'complete' state since last X seconds. Return.
                 */

                if (i == 1 && state.equals("complete")) {
                    //System.out.println();
                    return;
                }
                i++;
            }
            i = 0;
            oldstate = null;
            Thread.sleep(2000);

            /*
             * Now wait for state to become complete
             */
            while (true) {
                state = ((JavascriptExecutor) driver).executeScript("return document.readyState;").toString();
                //System.out.print("." + state.charAt(0) + ".");
                if (state.equals("complete"))
                    break;

                if (state.equals(oldstate))
                    i++;
                else
                    i = 0;
                /*
                 * If browser state is same (loading/interactive) since last 60
                 * secs. Refresh the page.
                 */
                if (i == 15 && state.equals("loading")) {
                    System.out.println("\nBrowser in " + state + " state since last 60 secs. So refreshing browser.");
                    driver.navigate().refresh();
                    System.out.print("Waiting for browser loading to complete");
                    i = 0;
                } else if (i == 6 && state.equals("interactive")) {
                    System.out.println(
                            "\nBrowser in " + state + " state since last 30 secs. So starting with execution.");
                    return;
                }

                Thread.sleep(4000);
                oldstate = state;

            }
            System.out.println();

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public void waitForElementToDisplay(By Selector) {
        int count = 0;
        WebElement element = getElement(Selector);
        while (!element.isDisplayed() && count < 20) {
            System.out.println("Waiting for element to display: " + Selector);
            count++;
            sleep(200);
        }
    }

    public void waitForElementTextToBeEmpty(WebElement element) {
        String text;
        try {
            text = element.getText();
            int maxRetries = 10;
            int retry = 0;
            while ((text.length() >= 1) || (retry < maxRetries)) {
                retry++;
                text = element.getText();
            }
        } catch (Exception e) {
            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
        }
    }

    public void waitForElementToBeClickable(By selector) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
        } catch (Exception e) {
            throw new TestException("The following element is not clickable: " + selector);
        }
    }

    public void waitForElementToBeVisible(By selector) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element was not visible: %s", selector));
        }
    }

    public void waitForElementToBeVisibleMayNull(By selector){
        try {
            wait = new WebDriverWait(driver, 1);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
        }
    }

    public void setCheckbox(By selector, String value){
        WebElement checkBox = getElement(selector);
        if(value.equals("YES")){
            if(!checkBox.isSelected())
                helper.myJavascripts().clickByJS(checkBox);
        }else{
            if(checkBox.isSelected())
                helper.myJavascripts().clickByJS(checkBox);
        }
    }
}
