package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import logger.MyLogger;

import static org.testng.Assert.assertTrue;
import static operation.ParallelTest.exception;

public class MyVerifys {
    AppiumDriver<MobileElement> driver;
    MyHelper helper;

    public MyVerifys(AppiumDriver<MobileElement> driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void verifyText(By selector, String expectedText){
        String actualText = helper.myBasics().getElement(selector).getText();
        if(!actualText.equals(expectedText)){
            helper.myTools().logFail("Text is not matched");
        }
        System.out.println("Text is verified!");
    }
}
