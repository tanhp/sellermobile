package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

public class MySwitchs {
    AppiumDriver<MobileElement> driver;
    MyHelper helper;

    public MySwitchs(AppiumDriver<MobileElement> driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    /* Make sure my app set setWebContentsDebuggingEnabled to true */
    public void switchContext(String contextName){
        Set<String> contexts = driver.getContextHandles();
        for(String s : contexts){
            if(s.contains(contextName))
                driver.context(s);
        }
    }
}
