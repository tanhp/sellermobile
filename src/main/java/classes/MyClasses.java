package classes;

public class MyClasses {

    private CProduct product;
    private CShopInfo shopInfo;
    private CPromotion promotion;
    private CChat chat;
    private CShippingSupport shipping;
    private CPrivateOffer privateOffer;
    private CSalesOrder salesOrder;
    private CCarrier carrier;
    private CBlog blog;
    private CBanner banner;

    public MyClasses(){
        product = new CProduct();
        shopInfo = new CShopInfo();
        promotion = new CPromotion();
        chat = new CChat();
        shipping = new CShippingSupport();
        privateOffer = new CPrivateOffer();
        salesOrder = new CSalesOrder();
        carrier = new CCarrier();
        blog = new CBlog();
        banner = new CBanner();
    }

    public void setProductObject(String object, String value){
        switch (object) {
            case "pg_ProductDetail_Category":
                product.setCatePath(value);
                break;
            case "pg_ProductDetail_CategoryPath":
                product.setCatePath2(value);
                break;
            case "pg_ProductDetail_ProductName":
            case "android_ProductDetail_ProductName":
            case "ios_ProductDetail_ProductName":
                product.setProductName(value);
                break;
            case "pg_ProductDetail_ProductSKU":
            case "android_ProductDetail_ProductSKU":
            case "ios_ProductDetail_ProductSKU":
                product.setProductSku(value);
                break;
            case "pg_ProductDetail_ProductPrice":
            case "android_ProductDetail_ProductPrice":
            case "ios_ProductDetail_ProductPrice":
                product.setProductPrice(value);
                break;
            case "pg_ProductDetail_InStock":
            case "android_ProductDetail_Instock":
            case "ios_ProductDetail_Instock":
                product.setInStock(value);
                break;
            case "pg_ProductDetail_Size":
                product.setProductSize(value);
                break;
            case "pg_ProductDetail_Color":
                product.setProductColor(value);
                break;
            case "pg_ProductList_InStock":
                product.setInStock(value);
                break;
            case "pg_ProductDetail_PromotionPrice":
                product.setDiscountMoney(value);
                break;
        }
    }

    public void setProductFromId(String value, String id){
        switch (value) {
            case "PRODUCT":
                product.setProductId(id);
                product.setBuyerUrl(id);
                break;
            case "PROMOTION":
                promotion.setPromotionId(id);
                break;
            case "BLOG":
                blog.setBlogId(id);
        }
    }

    public CProduct getProductObject(){
        return product;
    }

    public void setShopInfoObject(String object, String value){
        switch (object){
            case "pg_ShopInfo_ShopName":
            case "android_ShopInfo_ShopName":
            case "ios_ShopInfo_ShopName":
                shopInfo.setShopName(value);
                break;
            case "pg_ShopInfo_ShopUrl":
                shopInfo.setShopUrl(value);
                break;
            case "pg_ShopInfo_Slogan":
                shopInfo.setSlogan(value);
                break;
            case "pg_ShopInfo_ShortDescription":
                shopInfo.setShortDescription(value);
                break;
            case "pg_ShopInfo_SellerName":
            case "android_ShopInfo_SellerName":
            case "ios_ShopInfo_SellerName":
                shopInfo.setSellerName(value);
                break;
            case "pg_ShopInfo_Phone":
            case "android_ShopInfo_Phone":
            case "ios_ShopInfo_Phone":
                shopInfo.setPhone(value);
                break;
            case "pg_ShopInfo_Email":
            case "android_ShopInfo_Email":
            case "ios_ShopInfo_Email":
                shopInfo.setEmail(value);
                break;
            case "pg_ShopInfo_TaxCode":
            case "android_ShopInfo_TaxCode":
            case "ios_ShopInfo_TaxCode":
                shopInfo.setTaxCode(value);
                break;
            case "pg_ShopInfo_ShopAddress":
            case "android_ShopInfo_ShopAddress":
            case "ios_ShopInfo_ShopAddress":
                shopInfo.setShopAddress(value);
                break;
            case "pg_ShopInfo_ShopCity":
            case "android_ShopInfo_ShopCity":
            case "ios_ShopInfo_ShopCity":
                shopInfo.setShopCity(value);
                break;
            case "pg_ShopInfo_ShopDistrict":
            case "android_ShopInfo_ShopDistrict":
            case "ios_ShopInfo_ShopDistrict":
                shopInfo.setShopDistrict(value);
                break;
            case "pg_ShopInfo_ShopWard":
            case "android_ShopInfo_ShopWard":
            case "ios_ShopInfo_ShopWard":
                shopInfo.setShopWard(value);
                break;
        }
    }

    public CShopInfo getShopInfoObject(){
        return shopInfo;
    }

    public void setPromotionObject(String object, String value){
        switch (object){
            case "pg_Promotion_PromotionName":
                promotion.setPromotionName(value);
                break;
            case "pg_Promotion_DiscountAmount":
                if(Integer.valueOf(value) < 1000){
                    promotion.setDiscountPercent(value);
                    product.setDiscountPercent(value);
                }else{
                    promotion.setDiscountMoney(value);
                    product.setDiscountMoney(value);
                }


                break;
            case "pg_Promotion_PromotionDate":
                promotion.setPromotionDateTmp(value);
                break;
            case "pg_Promotion_PromotionDate1":
                promotion.setPromotionDate(value);
                break;
        }
    }

    public CPromotion getPromotionObject(){
        return promotion;
    }

    public void setChatObject(String object, String value){
        switch (object){
            case "pg_Chat_ChatIFrame":
                chat.setUserName(value);
                break;
            case "pg_ChatDetail_ChatTextBox":
            case "android_Chat_ChatTextBox":
                chat.setContentMessage(value);
                chat.setShortMessage(value);
                break;
        }
    }

    public CChat getChatObject(){
        return chat;
    }

    public void setShippingSupportObject(String object, String value){
        switch (object){
            case "pg_ShippingSupport_OrderAmount01":
                shipping.setOrderAmount01(value);
                break;
            case "pg_ShippingSupport_SellerSupportFee01":
                shipping.setSupportFee01(value);
                break;
            case "pg_ShippingSupport_Effect01":
                shipping.setEffect01(value);
                break;
            case "pg_ShippingSupport_OrderAmount02":
                shipping.setOrderAmount02(value);
                break;
            case "pg_ShippingSupport_SellerSupportFee02":
                shipping.setSupportFee02(value);
                break;
            case "pg_ShippingSupport_Effect02":
                shipping.setEffect02(value);
                break;
        }
    }
    public CShippingSupport getShippingSupport(){
        return shipping;
    }

    public void setPrivateOfferObject(String object, String value){
        privateOffer.setProductName(product.getProductName());
        privateOffer.setOriginalMoney(product.getProductPrice());
        switch (object){
            case "pg_PrivateOfferDetail_DiscountAmountPercent":
                privateOffer.setDiscountPercent(value);
                break;
            case "pg_PrivateOfferDetail_DiscountAmountMoney":
                privateOffer.setDiscountMoney(value);
                break;
            case "pg_PrivateOfferDetail_ExpireDate":
                privateOffer.setExpiredDate(value);
                break;
        }
    }

    public CPrivateOffer getPrivateOfferObject(){
        return privateOffer;
    }

    public void setSalesOrderObject(String object, String value){
        switch(object){
            case "pg_Buyer_OrderNumber":
                salesOrder.setOrderNumber(value);
                break;
        }
    }

    public CSalesOrder getSalesOrderObject(){
        return salesOrder;
    }

    public CCarrier getCarrierObject(){
        return carrier;
    }

    public void setCarrierObject(String keyword, String carrierName, String isChecked){
        if(!keyword.equals("VERIFY_NOSENMALLCARRIER") && carrierName.contains("ecom_shipping") && isChecked.equals("YES")){
            carrier.setCurrentCarrier(carrierName);
            carrier.setUrlAndCity();
        }

    }

    public CBlog getBlogObject(){
        return blog;
    }

    public void setBlogObject(String object, String value){
        switch(object){
            case "pg_BlogDetail_BlogName":
                blog.setName(value);
                break;
            case "pg_BlogDetail_BlogDescription":
                blog.setDescription(value);
                break;
            case "pg_BlogDetail_BlogTag":
                blog.setTag(value);
                break;
            case "pg_BlogDetail_BlogContent":
                blog.setContent(value);
                break;
        }
    }

    public CBanner getBannerObject(){
        return banner;
    }

    public void setBannerObject(String object, String value){
        switch (object){
            case "pg_BannerDetail_SystemURL":
                banner.setSystemURL(value);
                break;
        }
    }
}
