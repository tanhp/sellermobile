package classes;

public class CShippingSupport {
    private String orderAmount01;
    private String supportFee01;
    private String orderAmount02;
    private String supportFee02;
    private Boolean effect01;
    private Boolean effect02;

    public String getOrderAmount01() {
        return orderAmount01;
    }

    public void setOrderAmount01(String orderAmount01) {
        this.orderAmount01 = orderAmount01;
    }

    public String getSupportFee01() {
        return supportFee01;
    }

    public void setSupportFee01(String supportFee01) {
        this.supportFee01 = supportFee01;
    }

    public String getOrderAmount02() {
        return orderAmount02;
    }

    public void setOrderAmount02(String orderAmount02) {
        this.orderAmount02 = orderAmount02;
    }

    public String getSupportFee02() {
        return supportFee02;
    }

    public void setSupportFee02(String supportFee02) {
        this.supportFee02 = supportFee02;
    }

    public Boolean getEffect01() {
        return effect01;
    }

    public void setEffect01(String effect01) {
        if(effect01.equals("YES"))
            this.effect01 = true;
        else
            this.effect01 = false;
    }

    public Boolean getEffect02() {
        return effect02;
    }

    public void setEffect02(String effect02) {
        if(effect02.equals("YES"))
            this.effect02 = true;
        else
            this.effect02 = false;
    }
}
