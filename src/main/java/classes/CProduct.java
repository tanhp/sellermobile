package classes;

import utils.FormatUtils;

public class CProduct {
    private String productName;
    private String productSku;
    private String catePath;
    private String productColor;
    private String productSize;
    private String productPrice;
    private String discountPercent;
    private String discountMoney;
    private String discountPrice;
    private Boolean inStock;
    private String productId;
    private String buyerUrl;

    public String getProductName(){
        return productName;
    }

    public String getProductSku(){
        return productSku;
    }

    public String getCatePath(){
        return catePath;
    }

    public String getProductColor(){
        return productColor;
    }

    public String getProductSize(){
        return productSize;
    }

    public String getProductPrice(){
        return productPrice;
    }

    public String getDiscountPrice(){
        float discountPrice;
        if(this.discountPercent != null) {
            discountPrice = Long.valueOf(this.productPrice.replace(",", ""))
                    * (1 - (Long.valueOf(this.discountPercent) / 100.0f));
        }else{
            discountPrice = Long.valueOf(this.productPrice.replace(",","")) - Long.valueOf(this.discountMoney);
        }
        return FormatUtils.formatNumber(String.valueOf(discountPrice).replace(".0", ""), "#,000");
    }

    public Boolean getInStock(){
        return inStock;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductName(String productName){
        if(!productName.equals(""))
            this.productName = productName;
    }

    public void setProductSku(String productSku){
        if(!productSku.equals(""))
            this.productSku = productSku;
    }

    public void setCatePath(String catePath){
        //catePath = FormatUtils.matchAndReplaceNonEnglishChar(catePath) + "/";
        if(!catePath.equals(""))
            this.catePath = catePath;
    }

    public void setCatePath2(String catePath2){
        this.catePath = catePath2.replace(" > ",", ");
    }

    public void setProductColor(String productColor){
        this.productColor = productColor;
    }

    public void setProductSize(String productSize){
        this.productSize = productSize;
    }

    public void setProductPrice(String productPrice){
        if(!productPrice.equals("") && !productPrice.equals("NO")) {
            productPrice = productPrice + "0000";
            productPrice = productPrice.replace(",","");
            productPrice = FormatUtils.formatNumber(productPrice, "#0,000");
            this.productPrice = productPrice;
        }
    }

    public void setDiscountPrice(String discountPrice){

    }

    public void setInStock(String inStock){
        if(inStock.equals("YES") || inStock.equals("ON"))
            this.inStock = true;
        else
            this.inStock = false;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBuyerUrl() {
        return buyerUrl;
    }

    public void setBuyerUrl(String productId) {
        this.buyerUrl = "https://www.sendo.vn/" + FormatUtils.matchAndReplaceNonEnglishChar(productName) + "-" + productId + ".html";
        System.out.println(this.buyerUrl);
    }

    public void setDiscountPercent(String discountAmount){
        this.discountPercent = discountAmount;
    }

    public String getDiscountPercent(){
        return this.discountPercent;
    }

    public void setDiscountMoney(String discountMoney){
        this.discountMoney = discountMoney;
    }

    public String getDiscountMoney() {return this.discountMoney; }
}
