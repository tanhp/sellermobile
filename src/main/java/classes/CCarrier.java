package classes;

public class CCarrier {
    private String currentCarrier;
    private String url;
    private String city;

    public String getCurrentCarrier() {
        return currentCarrier;
    }

    public void setCurrentCarrier(String carrier){
        currentCarrier = carrier;
    }

    public void setUrlAndCity() {
        switch (currentCarrier){
            case "ecom_shipping_vnpt":
            case "ecom_shipping_ghn":
            case "ecom_shipping_dhl_pde":
            case "ecom_shipping_njv_standard":
                url = "https://www.sendo.vn/shop/shopbaby47/gift-voucher-100-11957823.html";
                city = "Hà Nội";
                break;
        }
    }

    public String getUrl(){
        return url;
    }

    public String getCity(){
        return city;
    }


}
