package constant;

public class MyConstants {

    /* PATH_CONSTANTS */

    public static String SYSTEM_PATH = System.getProperty("user.dir");


    public static String CONFIG_PATH = SYSTEM_PATH + "/configs/";
    public static String INPUT_PATH = SYSTEM_PATH + "/input/";
    public static String OUTPUT_PATH = SYSTEM_PATH + "/output/";
    public static String EXCEL_OUTPUT_PATH = OUTPUT_PATH + "excelOutput/";
    public static String IMAGES_PATH = SYSTEM_PATH + "/images/";
    public static String PROPERTIES_PATH = SYSTEM_PATH + "/properties/";
    public static String SCREENSHOT_PATH = SYSTEM_PATH + "/screenshots/";
    public static String HTMLREPORT_PATH = SYSTEM_PATH + "/htmlreports/";
    public static String LOG_PATH = SYSTEM_PATH + "/logs/log4j/";


    /* URL_CONSTANTS */

    public static String SERVER_URL = "http://172.30.119.110:81/smc";
    public static String SCREENSHOT_URL = SERVER_URL + "/screenshots/";
    public static String HTMLREPORT_URL = SERVER_URL + "/htmlreports/";

    /* DATABASE */
    public static String DB_URL = "jdbc:mysql://172.30.119.111";
    public static String DB_USERNAME = "autoUser";
    public static String DB_PASSWORD = "abcd@1234";
    public static String DB_DATABASENAME = "test";
    public static String DB_TABLENAME = "seller";

    /* ENVIRONMENT */
    public static String ENVIRONMENT = "Seller - Production";

    /* DOMAIN */

    public static String BASE_SELLER_URL = "https://ban.sendo.vn";
    public static String BASE_BUYER_URL = "https://sendo.vn";

    public static final String BROWSER = "chrome";

    // CONFIG
    public static int timezoneOffset = 6;

    // Buyer Account
    public static String BUYER_USERNAME="tester01gm@gmail.com";
    public static String BUYER_PASSWORD="123456";

    // Skype account
    public static String SKYPE_USERNAME="mon.mon236";
    public static String SKYPE_PASSWORD = "mon8245679311";
    public static String SKYPE_IDENTITY_TAN = "8:tanhopham1990";
    public static String SKYPE_IDENTITY_BICH = "8:bichbaby47";
    public static String SKYPE_IDENTITY_GROUP = "19:4899829c0e434b2e888c3b6eaae77797@thread.skype";

    // Appium Server
    private static String WIN_NODE_JS_PATH = "C:\\Program Files\\nodejs\\node.exe";
    private static String WIN_APPIUM_JS_PATH = "C:\\Users\\tanhp\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js";
    private static String MAC_NODE_JS_PATH = "/usr/local/bin/node";
    private static String MAC_APPIUM_JS_PATH = "/usr/local/lib/node_modules/appium/build/lib/main.js";

    public static final String IP_ADRESS = "127.0.0.1";
    public static final String LOG_LEVEL = "info";

    //public static final String PLATFORM = "Android";
    public static final String PLATFORM = "iOS";

    // Android & IOS Capabilities
    public static String ANDROID_DEVICE_UDID = "192.168.223.101:5555";
    public static String ANDROID_DEVICE_NAME = "192.168.248.101:5555";
    //public static String ANDROID_DEVICE_NAME = "192.168.56.101:5555";
    public static String ANDROID_PLATFORM_NAME = "Android";
    public static String ANDROID_PLATFORM_VERSION = "4.4.4";

    public static String IOS_DEVICE_UDID = "46898d5e0e69a5f39990bfffcb0f407bf906e063";
    public static String IOS_DEVICE_BUNDLEID = "com.sendo.sendoshop";
    public static String IOS_DEVICE_NAME = "iPhone 5S";
    public static String IOS_PLATFORM_NAME = "iOS";
    public static String IOS_PLATFORM_VERSION = "11.2";


    public static int newCommandTimeOut = 120;
    public static String appPackage = "com.sendoseller";
    public static String appActivity = "com.sendoseller.MasterActivity";
    public static String iosBundleId = "com.sendo.sendoshop";
    public static String buyerAppPackage = "com.sendo";
    public static String buyerAppActivity = "com.sendo.module.home.view.HomeActivity";

    public static String DRIVER_PATH = SYSTEM_PATH + "/drivers/";
    private static String WIN_CHROME_DRIVER_PATH = DRIVER_PATH + "chromedriver.exe";
    private static String MAC_CHROME_DRIVER_PATH = DRIVER_PATH + "chromedriver";

    public static String getNodeJsPath(){
        if(System.getProperty("os.name").startsWith("Windows"))
            return WIN_NODE_JS_PATH;
        else
            return MAC_NODE_JS_PATH;
    }

    public static String getAppiumJSPath(){
        if(System.getProperty("os.name").startsWith("Windows"))
            return WIN_APPIUM_JS_PATH;
        else
            return MAC_APPIUM_JS_PATH;
    }

    public static String getChromeDriverPath(){
        if(System.getProperty("os.name").startsWith("Windows"))
            return WIN_CHROME_DRIVER_PATH;
        else
            return MAC_CHROME_DRIVER_PATH;
    }

}
