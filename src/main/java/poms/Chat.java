package poms;

import classes.CChat;
import constant.MyConstants;
import helpers.MyHelper;
import helpers.MyWebDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Chat {
    MyHelper helper;
    MyWebDriver myDriver;
    CChat chat;

    public Chat(MyHelper helper){
        this.helper = helper;
        this.chat = helper.myObjects().getChatObject();
        this.myDriver = new MyWebDriver(helper);
    }

    public void verifyChat(){

        List<MobileElement> content = helper.myBasics().getElements(By.id("com.sendoseller:id/tv_seller_message"));
        String contentMessage = content.get(content.size()-1).getText();

        if(!chat.getContentMessage().equals(contentMessage)) {
            helper.myTools().logFail("Content message is not matched!");
        }
        System.out.println("Content message chat verified!");

        helper.myBasics().click(By.id("com.sendoseller:id/btn_back"));

        String username = helper.myBasics().getElements(By.id("com.sendoseller:id/tv_buyer_name")).get(0).getText();
        String shortMessage = helper.myBasics().getElements(By.id("com.sendoseller:id/tv_summary_content")).get(0).getText();

        if(!username.equals("Tan Ho")) {
            helper.myTools().logFail("Username is not matched!");
        }
        System.out.println("Username chat verified!");
        if(!chat.getShortMessage().equals(shortMessage)) {
            System.out.println("Actual short message: " + chat.getShortMessage());
            System.out.println("Expected short message: " + shortMessage);
            helper.myTools().logFail("Short message ist not matched!");
        }
        System.out.println("Short message chat verified!");
    }

    public void switchToChat(String user) {
        WebElement iChat = myDriver.getElement(By.xpath("//iframe[@title='Chat']"));
        myDriver.switchToNewFrame(iChat);
        myDriver.click(By.xpath("//div[contains(@class,'nameLeft') and text()='" + user + "']"));
    }

    public void verifyChatOnBuyer() {
        myDriver.navigateToURL("https://sendo.vn");
        myDriver.sleep(5000);
        myDriver.clickMayNull(By.xpath("//button[contains(@class,'CloseButton')]"));
        myDriver.click(By.xpath("//div[contains(@class,'floatingButton')]"));
        myDriver.sleep(1000);
        WebElement loginForm = myDriver.getElement(By.xpath("//div[text()='Đã có SendoID']"));
        if(loginForm != null && loginForm.isDisplayed()){
            myDriver.click(By.xpath("//div[text()='Đã có SendoID']"));
            myDriver.sendKeys(By.name("email"),MyConstants.BUYER_USERNAME);
            myDriver.sendKeys(By.name("password"),MyConstants.BUYER_PASSWORD);
            myDriver.click(By.xpath("//button[text()='Đăng nhập']"));
        }
        myDriver.waitForPageLoad();
        switchToChat("shopbaby47");
        List<WebElement> content = myDriver.getElements(By.xpath("//span[contains(@class,'message')]"));
        String contentMessage = content.get(content.size()-1).getText();
        if(!chat.getContentMessage().equals(contentMessage)) {
            helper.myTools().logFail("Content message is not matched!");
        }
        System.out.println("Content message chat verified!");
        myDriver.closeDriver();
    }

}
