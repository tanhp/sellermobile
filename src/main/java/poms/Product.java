package poms;

import classes.CProduct;
import helpers.MyHelper;
import helpers.MyWebDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Product {
    private MyHelper helper;
    private CProduct product;
    private MyWebDriver myDriver;


    public Product(MyHelper helper){
        this.helper = helper;
        this.product = helper.myObjects().getProductObject();
        this.myDriver = new MyWebDriver(this.helper);
    }

    public void chooseImageFromGallery(By selector, String images){
        String gallery = "com.sendoseller:id/img_gallery";
        String choose = "com.sendoseller:id/btn_choose";

        if(images.equals(""))
            return;
        helper.myBasics().click(selector);
        List<MobileElement> galleryList = helper.myBasics().getElements(By.id(gallery));
        String[] imageIndex = images.split(", ");
        for(int i=1; i<=imageIndex.length; i++){
            helper.myBasics().click(galleryList.get(Integer.valueOf(imageIndex[i - 1])));
        }
        helper.myBasics().click(By.id(choose));
    }

    public void chooseCategory(By selector, String value){
        helper.myBasics().click(selector);
        helper.myBasics().sendKeys(By.id("com.sendoseller:id/edt_search"),value);
        helper.myBasics().click(By.id("com.sendoseller:id/item_id"));
    }

    public void writeDescription(By selector, String value){
        String editor = "com.sendoseller:id/rtEditText";
        String done = "com.sendoseller:id/btn_done";
        helper.myBasics().click(selector);
        helper.myBasics().sendKeys(By.id(editor),value);
        helper.myBasics().click(By.id(done));
    }

    public void chooseAttributes(By selector){
        helper.myBasics().click(selector);
        helper.myBasics().sendKeys(By.id("com.sendoseller:id/tv_btn_input"),"ABCD");
        helper.myBasics().click(By.xpath("//android.widget.TextView[@text='HCM-Q2']"));
        helper.myBasics().click(By.id("com.sendoseller:id/btn_done"));
    }


    public void searchProductBySKU(String sku){
        if(sku.equals("NEWSKU"))
            sku = product.getProductSku();
        helper.myBasics().click(By.id("com.sendoseller:id/btn_search"));
        helper.myBasics().sendKeys(By.id("com.sendoseller:id/edt_search"),sku);
        helper.myKeyboards().pressAndroidKey("ENTER");
    }

    public void searchAndGoToDetail(String sku){
        searchProductBySKU(sku);
        helper.myBasics().click(By.id("com.sendoseller:id/itemProduct_linearLayout_content"));
    }

    public void verifyProductFullInfo(){
        searchProductBySKU(product.getProductSku());
        //searchProductBySKU("SKU20022019131353");
        String aProductSku = helper.myBasics().getElement(By.id("com.sendoseller:id/itemProduct_textView_code")).getText();
        String aProductName = helper.myBasics().getElement(By.id("com.sendoseller:id/itemProduct_textView_name")).getText() ;
        String sProductPrice = helper.myBasics().getElement(By.id("com.sendoseller:id/itemProduct_textView_priceSaleOff")).getText();
        String sProductStatus = helper.myBasics().getElement(By.id("com.sendoseller:id/itemProduct_textView_status")).getText();

        if(!aProductSku.equals("MSP: " + product.getProductSku())) {
            helper.myTools().logFail("Product SKU is not matched!");
        }
        System.out.println("Product SKU Verified!");
        if(!product.getProductName().equals(aProductName)) {
            helper.myTools().logFail("Product name is not matched!");
        }
        System.out.println("Product name Verified!");
        String productPrice = product.getProductPrice() + " đ";
        System.out.println(productPrice);
        if(!productPrice.equals(sProductPrice)) {
            helper.myTools().logFail("Product price is not matched!");
        }
        System.out.println("Product Price Verified!");
        if(!sProductStatus.equals("ĐÃ DUYỆT")){
            helper.myTools().logFail("Product status is not matched!");
        }
        System.out.println("Product Status Verified!");
    }

    public void verifyProductFullInfoOnBuyer(){
        myDriver.navigateToURL("https://sendo.vn");
        myDriver.sleep(5000);
        myDriver.clickMayNull(By.xpath("//button[contains(@class,'CloseButton')]"));
        myDriver.sendKeys(By.xpath("//input[@placeholder='Tìm kiếm trên Siêu chợ Sen Đỏ']"),product.getProductSku());
        //myDriver.sendKeys(By.xpath("//input[@placeholder='Tìm kiếm trên Siêu chợ Sen Đỏ']"),"SKU21022019103214");
        myDriver.click(By.xpath("//button[contains(@class,'searchButton')]"));
        myDriver.click(By.xpath("//div[contains(@class,'productCard')]"));

        String price = myDriver.getElement(By.xpath("//strong[contains(@class,'currentPrice')]")).getText();
        String name_sku = myDriver.getElement(By.xpath("//h1[contains(@class,'productName')]")).getText();


        String productNameSku = product.getProductName() + " - " + product.getProductSku();
        if(!productNameSku.equals(name_sku)) {
            helper.myTools().logFail("Product name & SKU buyer are not matched!");
        }
        System.out.println("Product name and sku buyer verified!");
        String temp_productPrice = product.getProductPrice() + " Đ";
        if(!temp_productPrice.equals(price)) {
            helper.myTools().logFail("Product price is not matched!");
        }
        System.out.println("Product price buyer verified!");

        WebElement buttonOutstock = myDriver.getElementMayNull(By.xpath("//button[contains(@class,'btn-disabled')]"));
        if(!product.getInStock()){
            if(buttonOutstock == null || !buttonOutstock.isDisplayed()) {
                helper.myTools().logFail("Outstock button is not displayed when seller uncheck instock in shop!");
            }

        }else{
            if(buttonOutstock != null && buttonOutstock.isDisplayed()) {
                helper.myTools().logFail("Outstock button is isplayed when seller check instock in shop!");
            }
        }
        System.out.println("Product Instock Buyer Verified!");
        myDriver.closeDriver();
    }
}
