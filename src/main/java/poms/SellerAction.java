package poms;

import helpers.MyHelper;
import logger.MyLogger;
import operation.AndroidUIOperation;
import operation.IOSUIOperation;
import operation.UIOperation;
import utils.XLSWorker;

public class SellerAction {
    private UIOperation action;
    private String testAction;
    private MyHelper helper;

    public SellerAction(MyHelper helper) {
        this.helper = helper;
    }

    public void initData(String platform){
        if(platform.equals("Android")) {
            this.action = new AndroidUIOperation(helper);
            testAction = "android\\TestAction.xlsx";
        }else if(platform.equals("iOS")) {
            this.action = new IOSUIOperation(helper);
            testAction = "ios\\TestAction.xlsx";
        }
    }

    public void getLogin(String platform) {
        initData(platform);
        Object[][] objects = XLSWorker.getDataForUAT(testAction, "Login");

        if (objects == null)
            return;
        for (int i = 0; i < objects.length; i++) {
            String keyword = objects[i][1].toString();
            String object = objects[i][2].toString();
            String value = objects[i][3].toString();
            MyLogger.info(keyword + " - " + object + " - " + value);
            action.perform("\\android\\Login.properties", keyword, object, value);
        }
    }

    public void getNewProduct(String platform){
        initData(platform);
        Object[][] objects = XLSWorker.getDataForUAT(testAction, "AddNewProduct");
        if (objects == null)
            return;
        for (int i = 0; i < objects.length - 1; i++) {
            String keyword = objects[i][1].toString();
            String object = objects[i][2].toString();
            String value = objects[i][3].toString();
            MyLogger.info(keyword + " - " + object + " - " + value);
            action.perform("\\android\\Product.properties", keyword, object, value);
        }

    }
}
