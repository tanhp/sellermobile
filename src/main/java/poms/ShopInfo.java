package poms;

import classes.CShopInfo;
import helpers.MyHelper;
import helpers.MyWebDriver;
import org.openqa.selenium.By;

public class ShopInfo {
    private MyHelper helper;
    private CShopInfo profile;
    private MyWebDriver myDriver;


    public ShopInfo(MyHelper helper){
        this.helper = helper;
        this.profile = helper.myObjects().getShopInfoObject();
        this.myDriver = new MyWebDriver(this.helper);
    }

    public void verifyShopInfoOnBuyer(){
        myDriver.navigateToURL("https://www.sendo.vn/shop/" + profile.getShopName());
        myDriver.click(By.xpath("//a[text()='Thông Tin Shop']"));
        String name = myDriver.getElement(By.xpath("//span[contains(@class,'shop-name')]")).getText();
        String address = myDriver.getElement(By.xpath("//label[text()='Địa chỉ:']/following-sibling::span")).getText();
        String phone = myDriver.getElement(By.xpath("//label[text()='Điện thoại:']/following-sibling::span")).getText();
        String email = myDriver.getElement(By.xpath("//label[text()='Email:']/following-sibling::span")).getText();
        System.out.println(profile.getShopName());
        System.out.println(name);
        if(!profile.getShopName().equals(name)) {
            helper.myTools().logFail("Shop name is not matched!");
        }
        System.out.println("Shop name verified!");
        String fullAddress = profile.getShopAddress()
                + ", phường(xã) "
                + profile.getShopWard()
                + ", "
                + profile.getShopDistrict()
                + ", "
                + profile.getShopCity();
        if(!fullAddress.equals(address)) {
            helper.myTools().logFail("Shop address is not matched!");
        }
        System.out.println("Shop Address verified!");
        if(!profile.getPhone().equals(phone)) {
            helper.myTools().logFail("Shop phone is not matched!");
        }
        System.out.println("Shop phone verified!");
        if(!profile.getEmail().equals(email)) {
            helper.myTools().logFail("Shop email is not matched!");
        }
        System.out.println("Shop email verified!");
    }

}
