package poms;

import classes.CSalesOrder;
import constant.MyConstants;
import helpers.MyHelper;
import helpers.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SalesOrder {
    private MyHelper helper;
    private CSalesOrder salesOrder;
    private MyWebDriver myDriver;


    public SalesOrder(MyHelper helper){
        this.helper = helper;
        this.salesOrder = helper.myObjects().getSalesOrderObject();
        this.myDriver = new MyWebDriver(this.helper);
    }

    public void searchOrder(){
        helper.myBasics().click(By.id("com.sendoseller:id/btn_search"));
        helper.myBasics().click(By.id("com.sendoseller:id/btn_search"));
        helper.myBasics().sendKeys(By.id("com.sendoseller:id/edt_search"), salesOrder.getOrderNumber());
        helper.myBasics().click(By.id("com.sendoseller:id/txt_order_id"));
    }

    public void createOrder(){
        myDriver.navigateToURL("https://www.sendo.vn/gift-voucher-100-16221599.html");
        myDriver.sleep(10000);
        myDriver.clickMayNull(By.xpath("//button[contains(@class,'CloseButton')]"));
        myDriver.click(By.xpath("//button[contains(@class,'buynow')]"));
        myDriver.sleep(1000);
        WebElement loginForm = myDriver.getElement(By.xpath("//div[text()='Đã có SendoID']"));
        if(loginForm != null && loginForm.isDisplayed()){
            myDriver.click(By.xpath("//div[text()='Đã có SendoID']"));
            myDriver.sendKeys(By.name("email"),MyConstants.BUYER_USERNAME);
            myDriver.sendKeys(By.name("password"),MyConstants.BUYER_PASSWORD);
            myDriver.click(By.xpath("//button[text()='Đăng nhập']"));
        }
        myDriver.waitForPageLoad();
        myDriver.sleep(1000);
        myDriver.setCheckbox(By.xpath("//input[@value='ecom_shipping_dispatch_cptc']"),"YES");
        myDriver.sleep(1500);
        myDriver.click(By.xpath("//span[text()='Đặt hàng']"));
        myDriver.waitForPageLoad();
        salesOrder.setOrderNumber(myDriver.getElement(By.xpath("//span[text()='Mã đơn hàng:']/following-sibling::strong")).getText());
    }
}
