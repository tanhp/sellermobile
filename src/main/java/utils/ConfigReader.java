package utils;

import constant.MyConstants;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ConfigReader {
    public static String fileRead(String fileName, String objKey) {
        FileInputStream fis;
        try {
            fis = new FileInputStream(MyConstants.PROPERTIES_PATH + fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Cant't read config.properties file!");
            return "";
        }
        Properties p = new Properties();
        try {
            p.load(new InputStreamReader(fis,Charset.forName("UTF-8")));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cant't read config.properties file!");
            return "";
        }
        String objValue = p.getProperty(objKey);
        return objValue;
    }
}
