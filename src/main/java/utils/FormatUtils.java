package utils;

import java.text.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class FormatUtils {

    public static String formatDate(Date date, String format) {
        DateFormat simpleDate = new SimpleDateFormat(format);
        String formattedDate = simpleDate.format(date);
        return formattedDate;
    }

    public static String getCurrentTimeByTimezoneOffset(int timezoneOffset, String format) {
        ZoneOffset offset = ZoneOffset.ofHours(timezoneOffset);
        OffsetDateTime odt = OffsetDateTime.now(offset);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        String output = odt.format(formatter);
        return output;
    }

    public static String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }
    
    public static String getCurrentDate() {
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Calendar c = Calendar.getInstance(); 
        c.setTime(date); 
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        String output = formatter.format(date);
        return output;
    }
    
    public static String getToday() {
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }

    public static Integer GenerateNumber(int limit)
    {
        Random rand = new Random();
        int Low = 1;
        int High = limit;
        int Result = rand.nextInt(High-Low) + Low;
        return Result;
    }

    /*public String getCurrentDateTime(){
        Calendar cal = Calendar.getInstance();
        String time = cal.getTime().toString().replace(MyConstants.SLASH,"-").replace(MyConstants.SPACE,"_").replace(":","-");
        MyLogger.info("Current time is:" + MyConstants.SPACE + time);
        return time;
    }*/

    public static String matchAndReplaceNonEnglishChar(String subjectString)
    {
        subjectString = Normalizer.normalize(subjectString, Normalizer.Form.NFD);
        String resultString = subjectString.replaceAll("[^\\x00-\\x7F]", "").replaceAll(" ","-").replaceAll(",-","/").toLowerCase();
        return resultString;
    }

    public static String formatNumber(String number, String format){
        NumberFormat formatter = new DecimalFormat(format);
        return formatter.format(Long.valueOf(number));
    }


}
