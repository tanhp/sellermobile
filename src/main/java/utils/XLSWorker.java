package utils;

import constant.MyConstants;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XLSWorker {
    private static FileInputStream fis = null;
    private static FileOutputStream fos = null;
    private static XSSFWorkbook workbook = null;
    private static XSSFSheet sheet = null;
    private XSSFRow row = null;
    private XSSFCell cell = null;
    private static String sheetName = null;
    
	public static List<String>FSheets = new ArrayList<String>();
	public static List<Integer>FRows = new ArrayList<Integer>();
	public static List<Integer>FCols = new ArrayList<Integer>();
	public static List<String>FUrl = new ArrayList<String>();

    public XLSWorker(){

    }

    public static XSSFWorkbook createWorkbook(){
        try{
            return new XSSFWorkbook();
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public static XSSFSheet createSheet(XSSFWorkbook workbook, String sheetName){
        try{
            if(workbook==null)
                return null;
            return workbook.createSheet(sheetName);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public static XSSFWorkbook getWorkbook(String filePath){
        try {
            fis = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fis);
            //sheet = workbook.getSheetAt(0);
            fis.close();
            return workbook;
        } catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + filePath + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }

    public static XSSFSheet getSheet(XSSFWorkbook workbook, String sheetName){
        if(workbook==null)
            return null;
        try{
            return workbook.getSheetAt(workbook.getSheetIndex(sheetName));
        }catch (IllegalArgumentException illegal) {
            illegal.printStackTrace();
            System.out.println("Didn't find sheet name " + sheetName + ". Please check sheet name.");
        }
        return null;
    }


    public static Object[][] getDataForTestCase(String fileAction, String fileData, String sheetName, int dataRowNum){
        Object[][] objects = null;

        XSSFSheet actionSheet = getSheet(getWorkbook(MyConstants.INPUT_PATH + fileAction),sheetName);
        if(actionSheet==null)
            return null;
        XSSFSheet dataSheet = getSheet(getWorkbook(MyConstants.INPUT_PATH + fileData),sheetName);
        if(dataSheet==null)
            return null;

        int totalActionRows = actionSheet.getLastRowNum() - actionSheet.getFirstRowNum();
        System.out.println("Total rows: " + totalActionRows);
        objects = new Object[totalActionRows-1][4];

        int count = 1;
        System.out.println("Start getting values from excel file");
        for(int i=2; i<=totalActionRows; i++){
            objects[i-2][0] = actionSheet.getRow(i).getCell(0);
            objects[i-2][1] = actionSheet.getRow(i).getCell(1);
            objects[i-2][2] = actionSheet.getRow(i).getCell(2);
            objects[i-2][3] = actionSheet.getRow(i).getCell(3);
            if(actionSheet.getRow(i).getCell(4).getStringCellValue().equals("YES")) {
                objects[i-2][3] = dataSheet.getRow(dataRowNum).getCell(count);
                count++;
            }
        }
        return objects;
    }

    public static Object[][] getDataForUAT(String fileAction, String sheetName){
        Object[][] objects = null;

        XSSFSheet actionSheet = getSheet(getWorkbook(MyConstants.INPUT_PATH + fileAction),sheetName);
        if(actionSheet==null)
            return null;

        int totalActionRows = actionSheet.getLastRowNum() - actionSheet.getFirstRowNum();
        System.out.println("Total rows: " + totalActionRows);
        objects = new Object[totalActionRows-1][4];

        for(int i=2; i<=totalActionRows; i++){
            objects[i-2][0] = actionSheet.getRow(i).getCell(0);
            objects[i-2][1] = actionSheet.getRow(i).getCell(1);
            objects[i-2][2] = actionSheet.getRow(i).getCell(2);
            objects[i-2][3] = actionSheet.getRow(i).getCell(3);
        }
        return objects;
    }

    public static void writeExcel(XSSFWorkbook workbook){
        for (int i = workbook.getNumberOfSheets() - 1; i >= 0; i--)
            if (!workbook.getSheetName(i).contentEquals(sheetName))
                workbook.removeSheetAt(i);
        try{
            fos = new FileOutputStream(new File(MyConstants.EXCEL_OUTPUT_PATH + "result_TestValidation_" + sheetName + ".xlsx"));
            workbook.write(fos);
            fos.close();
            System.out.println("Excel written completed. Check output at " + MyConstants.EXCEL_OUTPUT_PATH);
        }catch (Exception e){
            System.out.println("Please close the result file before testing!");
        }
    }

    public static void writeCategoryExcel(XSSFWorkbook workbook){
        try{
            fos = new FileOutputStream(new File(MyConstants.OUTPUT_PATH + "Category.xlsx"));
            workbook.write(fos);
            fos.close();
            System.out.println("Excel written completed. Check output at " + MyConstants.OUTPUT_PATH);
        }catch (Exception e){
            System.out.println("Please close the result file before testing!");
        }

    }

    public static void writeHeaderFile(XSSFWorkbook workbook){
        try{
            fos = new FileOutputStream(new File(MyConstants.EXCEL_OUTPUT_PATH + "0Header.xlsx"));
            workbook.write(fos);
            fos.close();
            System.out.println("Excel written completed. Check output at " + MyConstants.EXCEL_OUTPUT_PATH);
        }catch (Exception e){
            System.out.println("Please close the result file before testing!");
        }
    }

    public static void updateExcel(XSSFSheet sheet, String cateUrl, String cateName, String cateAttributes){

        XSSFRow row = sheet.getRow(sheet.getLastRowNum());
        if(row == null){
            row = sheet.createRow(sheet.getLastRowNum());
        }else{
            row = sheet.createRow(sheet.getLastRowNum()+1);
        }


        XSSFCell cellCateUrl = row.createCell(0);
        XSSFCell cellCateName = row.createCell(1);
        XSSFCell cellCateAttributes = row.createCell(2);

        cellCateUrl.setCellValue(cateUrl);
        cellCateName.setCellValue(cateName);
        cellCateAttributes.setCellValue(cateAttributes);
    }

    public static void updateExcel(XSSFSheet sheet, Boolean status, String productId){
        XSSFRow row = sheet.getRow(sheet.getLastRowNum());
        XSSFCell cellResult = row.createCell(3);
        XSSFCell cellProductId = row.createCell(4);
        cellResult.setCellValue(status);
        cellProductId.setCellValue(productId);
    }

    public static void updateExcel(XSSFSheet sheet, int rowNum, String result, String screenshotUrl){
        XSSFCell cell = null;
        int colNum;
        XSSFRow row = sheet.getRow(rowNum);
        if(row == null)
            row = sheet.createRow(rowNum);
        cell = row.getCell(row.getLastCellNum());
        if(cell == null){
            cell = row.createCell(row.getLastCellNum());
        }
        cell.setCellValue(result);
        if(result.contains("FAILED")) {
        	colNum = row.getLastCellNum();
            cell = row.createCell(colNum);
        	cell.setCellValue("Screenshot");
        	FSheets.add(sheet.getSheetName());
        	FRows.add(rowNum);
        	FCols.add(colNum);
        	FUrl.add(screenshotUrl);
        }
    }

    public static void updateHeaderResult(XSSFSheet sheet, int rowNum, int noOfPass, int noOfFail){
        XSSFRow row = sheet.getRow(rowNum);
        XSSFCell cellPass = null;
        XSSFCell cellFail = null;
        cellPass = row.getCell(1);
        if(cellPass == null)
            cellPass = row.createCell(1);
        cellPass.setCellValue(noOfPass);
        cellFail = row.getCell(2);
        if(cellFail == null)
            cellFail = row.createCell(2);
        cellFail.setCellValue(noOfFail);
    }

    public static void updateFinalResult(XSSFSheet sheet, int rowNum, String result){
        XSSFCell cell = null;
        XSSFRow row = sheet.getRow(rowNum);
        cell = row.getCell(row.getLastCellNum());
        cell.setCellValue(result);
    }

    public static void setSheetName(String str_sheetName){
        sheetName = str_sheetName;
    }









    public static void mergeExcelFiles(File file) throws IOException {
        XSSFWorkbook book = new XSSFWorkbook();
        System.out.println(file.getName());
        String directoryName = MyConstants.EXCEL_OUTPUT_PATH;
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file1 : fList){
            if (file1.isFile()){
                String ParticularFile = file1.getName();
                FileInputStream fin = new FileInputStream(new File(directoryName+"\\"+ParticularFile));
                XSSFWorkbook b = new XSSFWorkbook(fin);
                for (int i = 0; i < b.getNumberOfSheets(); i++) {
                    XSSFSheet sheet = book.createSheet(b.getSheetName(i));
                    copySheets(book, sheet, b.getSheetAt(i));
                    System.out.println("Copying..");
                }
            }
            try {
                writeFile(book, file);
                if(!file1.getName().equals("0Header.xlsx"))
                    deleteFile(file1);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    protected static void writeFile(XSSFWorkbook book, File file) throws Exception {
        FileOutputStream out = new FileOutputStream(file);
        book.write(out);
        out.close();
    }
    private static void copySheets(XSSFWorkbook newWorkbook, XSSFSheet newSheet, XSSFSheet sheet){
        copySheets(newWorkbook, newSheet, sheet, true);
    }

    private static void copySheets(XSSFWorkbook newWorkbook, XSSFSheet newSheet, XSSFSheet sheet, boolean copyStyle){
        int newRownumber = newSheet.getLastRowNum();
        int maxColumnNum = 0;
        Map<Integer, XSSFCellStyle> styleMap = (copyStyle) ? new HashMap<Integer, XSSFCellStyle>() : null;

        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            XSSFRow srcRow = sheet.getRow(i);
            XSSFRow destRow = newSheet.createRow(i + newRownumber);
            if (srcRow != null) {
                copyRow(newWorkbook, sheet, newSheet, srcRow, destRow, styleMap);
                if (srcRow.getLastCellNum() > maxColumnNum) {
                    maxColumnNum = srcRow.getLastCellNum();
                }
            }
        }
        for (int i = 0; i <= maxColumnNum; i++) {
            newSheet.setColumnWidth(i, sheet.getColumnWidth(i));
        }
    }

    public static void copyRow(XSSFWorkbook newWorkbook, XSSFSheet srcSheet, XSSFSheet destSheet, XSSFRow srcRow, XSSFRow destRow, Map<Integer, XSSFCellStyle> styleMap) {
        destRow.setHeight(srcRow.getHeight());
        for (int j = srcRow.getFirstCellNum(); j <= srcRow.getLastCellNum(); j++) {
            XSSFCell oldCell = srcRow.getCell(j);
            XSSFCell newCell = destRow.getCell(j);
            if (oldCell != null) {
                if (newCell == null) {
                    newCell = destRow.createCell(j);
                }
                copyCell(newWorkbook, oldCell, newCell, styleMap);
            }
        }
    }

    public static void copyCell(XSSFWorkbook newWorkbook, XSSFCell oldCell, XSSFCell newCell, Map<Integer, XSSFCellStyle> styleMap) {
        if(styleMap != null) {
            int stHashCode = oldCell.getCellStyle().hashCode();
            XSSFCellStyle newCellStyle = styleMap.get(stHashCode);
            if(newCellStyle == null){
                newCellStyle = newWorkbook.createCellStyle();
                newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
                styleMap.put(stHashCode, newCellStyle);
            }
            newCell.setCellStyle(newCellStyle);
        }
        switch(oldCell.getCellType()) {
            case XSSFCell.CELL_TYPE_STRING:
                newCell.setCellValue(oldCell.getRichStringCellValue());
                break;
            case XSSFCell.CELL_TYPE_NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case XSSFCell.CELL_TYPE_BLANK:
                newCell.setCellType(XSSFCell.CELL_TYPE_BLANK);
                break;
            case XSSFCell.CELL_TYPE_BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case XSSFCell.CELL_TYPE_ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
                break;
            case XSSFCell.CELL_TYPE_FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            default:
                break;
        }
    }

    public static void deleteFile(File file){
        try{
            if(file.delete()){
                System.out.println(file.getName() + " is deleted!");
            }else{
                System.out.println("Delete operation is failed.");
            }

        }catch(Exception e){

            e.printStackTrace();

        }
    }
}
