package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import constant.MyConstants;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellFill;


public class ExcelMerger {

	public static List<String> getListOfSheets(File file) throws IOException {
        List<String> listOfSheets = new ArrayList<>();
        String directoryName = MyConstants.EXCEL_OUTPUT_PATH;
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file1 : fList){
            if (file1.isFile()){
                String ParticularFile = file1.getName();
                FileInputStream fin = new FileInputStream(new File(directoryName+"\\"+ParticularFile));
                XSSFWorkbook b = new XSSFWorkbook(fin);
                for (int i = 0; i < b.getNumberOfSheets(); i++) {
                    listOfSheets.add(b.getSheetName(i));
                }
            }
        }
        return listOfSheets;
    }
	
    public static void mergeExcelFiles(File file) throws IOException {
        XSSFWorkbook book = new XSSFWorkbook();
        System.out.println(file.getName());
        String directoryName = MyConstants.EXCEL_OUTPUT_PATH;
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file1 : fList){
            if (file1.isFile()){
                String ParticularFile = file1.getName();
                FileInputStream fin = new FileInputStream(new File(directoryName + ParticularFile));
                XSSFWorkbook b = new XSSFWorkbook(fin);
                for (int i = 0; i < b.getNumberOfSheets(); i++) {
                    XSSFSheet sheet = book.createSheet(b.getSheetName(i));
                    copySheets(book, sheet, b.getSheetAt(i));
                    System.out.println("Copying..");
                }
            }
            try {
                writeFile(book, file);
                if(!file1.getName().equals("0Header.xlsx"))
                    deleteFile(file1);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    protected static void writeFile(XSSFWorkbook book, File file) throws Exception {
        FileOutputStream out = new FileOutputStream(file);
        book.write(out);
        out.close();
    }
    private static void copySheets(XSSFWorkbook newWorkbook, XSSFSheet newSheet, XSSFSheet sheet){
        copySheets(newWorkbook, newSheet, sheet, true);
    }

    private static void copySheets(XSSFWorkbook newWorkbook, XSSFSheet newSheet, XSSFSheet sheet, boolean copyStyle){
        int newRownumber = newSheet.getLastRowNum();
        int maxColumnNum = 0;
        Map<Integer, XSSFCellStyle> styleMap = (copyStyle) ? new HashMap<Integer, XSSFCellStyle>() : null;

        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            XSSFRow srcRow = sheet.getRow(i);
            XSSFRow destRow = newSheet.createRow(i + newRownumber);
            if (srcRow != null) {
                copyRow(newWorkbook, sheet, newSheet, srcRow, destRow, styleMap);
                if (srcRow.getLastCellNum() > maxColumnNum) {
                    maxColumnNum = srcRow.getLastCellNum();
                }
            }
        }
        for (int i = 0; i <= maxColumnNum; i++) {
            newSheet.setColumnWidth(i, sheet.getColumnWidth(i));
        }
    }

    public static void copyRow(XSSFWorkbook newWorkbook, XSSFSheet srcSheet, XSSFSheet destSheet, XSSFRow srcRow, XSSFRow destRow, Map<Integer, XSSFCellStyle> styleMap) {
        destRow.setHeight(srcRow.getHeight());
        for (int j = srcRow.getFirstCellNum(); j <= srcRow.getLastCellNum(); j++) {
            XSSFCell oldCell = srcRow.getCell(j);
            XSSFCell newCell = destRow.getCell(j);
            if (oldCell != null) {
                if (newCell == null) {
                    newCell = destRow.createCell(j);
                }
                copyCell(newWorkbook, oldCell, newCell, styleMap);
            }
        }
    }

    public static void copyCell(XSSFWorkbook newWorkbook, XSSFCell oldCell, XSSFCell newCell, Map<Integer, XSSFCellStyle> styleMap) {
        if(styleMap != null) {
            int stHashCode = oldCell.getCellStyle().hashCode();
            XSSFCellStyle newCellStyle = styleMap.get(stHashCode);
            //XSSFCellStyle newCellStyle = newCell.getSheet().getWorkbook().createCellStyle();
            if (newCellStyle == null) {
                newCellStyle = newWorkbook.createCellStyle();
                newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
                //newCellStyle.getCoreXf().unsetBorderId();
                //newCellStyle.getCoreXf().unsetFillId();
            }
            newCell.setCellStyle(newCellStyle);
        }

        StylesTable newStylesSource = newCell.getSheet().getWorkbook().getStylesSource();
        StylesTable oldStylesSource = oldCell.getSheet().getWorkbook().getStylesSource();
        for (XSSFCellFill fill : oldStylesSource.getFills()) {
            XSSFCellFill fillNew = new XSSFCellFill(fill.getCTFill());
            newStylesSource.putFill(fillNew);
        }
        for (XSSFCellBorder border : oldStylesSource.getBorders()) {
            XSSFCellBorder borderNew = new XSSFCellBorder(border.getCTBorder());
            newStylesSource.putBorder(borderNew);
        }


        switch(oldCell.getCellType()) {
            case XSSFCell.CELL_TYPE_STRING:
                newCell.setCellValue(oldCell.getRichStringCellValue());
                break;
            case XSSFCell.CELL_TYPE_NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case XSSFCell.CELL_TYPE_BLANK:
                newCell.setCellType(XSSFCell.CELL_TYPE_BLANK);
                break;
            case XSSFCell.CELL_TYPE_BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case XSSFCell.CELL_TYPE_ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
                break;
            case XSSFCell.CELL_TYPE_FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            default:
                break;
        }
    }

    public static void deleteFile(File file){
        try{
            if(file.delete()){
                System.out.println(file.getName() + " is deleted!");
            }else{
                System.out.println("Delete operation is failed.");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            mergeExcelFiles(new File(MyConstants.OUTPUT_PATH + "NewXlsfileWhereDataWillBeMerged.xlsx"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
