package operation;

import helpers.MyHelper;
import org.openqa.selenium.By;

public class AndroidUIOperation extends UIOperation {

    public AndroidUIOperation(MyHelper helper){
        super(helper);
    }

    public void perform(String fileProperties, String keyword, String object, String value){
        super.perform(fileProperties,keyword,object,value);
        By selector = null;
        if(!object.equals("")){
            if(!object.contains("ecom_shipping") && !object.contains("BlogContent"))
                selector = getObject(fileProperties,object);
        }

        switch(keyword){
            case "LOGIN":
                helper.sellerAction().getLogin("Android");
                break;
            case "ADDNEWPRODUCT":
                helper.sellerAction().getNewProduct("Android");
                break;
            case "ADDPHOTOSGALLERY":
                helper.product().chooseImageFromGallery(selector,value);
                break;
            case "CHOOSECATEGORY":
                if(!value.equals("")){
                    helper.product().chooseCategory(selector, value);
                }
                break;
            case "WRITEDESCRIPTION":
                if(!value.equals("")){
                    helper.product().writeDescription(selector,value);
                }
                break;
            case "CHOOSEATTRIBUTES":
                helper.product().chooseAttributes(selector);
                break;
            case "SETDATE":
                if(!value.equals("")) {
                    helper.myBasics().setDate(selector);
                }
                break;
            case "SETSWITCH":
                helper.myBasics().setSwitch(selector,value);
                break;
            case "SETCOMBOBOX":
                helper.myBasics().setComboBox(selector,value);
                break;
            case "SHORTSWIPETOBOT":
                helper.mySwipes().shortSwipeToBot();
                break;
            case "SHORTSWIPETOTOP":
                helper.mySwipes().shortSwipeToTop("Android");
                break;
            case "SLEEP":
                helper.myBasics().sleep(Long.valueOf(value));
                break;
            case "SWIPEELEMENTTOLEFT":
                helper.mySwipes().swipeElementToLeft(selector);
                break;
            case "VERIFYCHAT":
                helper.chat().verifyChat();
                break;
            case "VERIFYPRODUCTINFO":
                helper.product().verifyProductFullInfo();
                break;
            case "SEARCHPRODUCT":
                helper.product().searchProductBySKU(value);
                break;
            case "SEARCH_GOTO_PRODUCTDETAIL":
                helper.product().searchAndGoToDetail(value);
                break;
            case "SEARCHORDER":
                helper.salesOrder().searchOrder();
                break;
        }
    }
}
