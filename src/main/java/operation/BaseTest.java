package operation;

import constant.MyConstants;

import helpers.MyHelper;
import logger.MyLogger;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class BaseTest extends ParallelTest{
    @Parameters("platform")
    @BeforeSuite
    public void suiteSetUp(ITestContext iSuite, @Optional(MyConstants.PLATFORM) String platform){
    	String testSuiteName = iSuite.getCurrentXmlTest().getSuite().getName();
        MyLogger.info("Start test suite: " + testSuiteName);
        setUpReport(iSuite);
        //setUpDatabase();
        startAppiumServer(platform);
        setDriver(startAppiumServerInParallel(platform));
    }


    @BeforeClass
    public void classSetUp(){

    }


    @Parameters("platform")
    @BeforeMethod(alwaysRun = true)
    public void testCaseSetUp(Method methodName, @Optional(MyConstants.PLATFORM) String platform){
    	MyLogger.info("Running testcase: " + methodName.getName());
    	launchDriver();
        setHelper(new MyHelper(getDriver()));
        setUpTestCase(methodName);
    }

    @AfterMethod
    public void testCaseTearDown(ITestResult iResult, ITestContext iContext){
        updateTestResult(iResult);
        insertIntoReport();
        //insertIntoDatabase(iResult,iContext);
        //sendToSlack();
        //sendToEmail();
        closeDriver();
    }

    @AfterClass
    public void classTearDown(){

    }

    //@AfterSuite
    public void suiteTearDown(ITestContext testSuite){
        killAppiumServer();
        closeReport();
        //closeDatabase();
        //insertIntoHeaderFile(testSuite);
        insertIntoJSONFile(testSuite);
    }
}
