package operation;


import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;

import constant.MyConstants;
import database.Database;
import helpers.MyHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import logger.MyLogger;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.Parameters;
import report.ExtentReport;
import utils.FormatUtils;
import utils.SkypeSender;
import utils.XLSWorker;

import java.awt.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.List;

public class ParallelTest {

    private AppiumDriver<MobileElement> driver;
    private MyHelper helper;

    XSSFWorkbook workbook = null;
    XSSFWorkbook workbook1 = null;
    private String fileValidation = null;
    String sheetName = null;
    int rowNum = -1;

    UIOperation action;
    ExtentReport report;
    public static Method method;
    String reportPath = null;
    String reportUrl = null;
    private String id = null;
    private String executionDate = null;
    private String executionTime = null;
    private String executionTimeSuite = null;

    private String testResult = null;
    Database database;
    private List<String> columns = Arrays.asList("Run_ID", "Test_suite", "Test_method", "Execution_time",
            "Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report", "Error_screenshot", "Environment");
    private List<String> columns2 = Arrays.asList("Environment","Test_suite",	"Test_method", "Running_status", "Execution_time", 
    		"Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report", "Error_screenshot");
    private List<String> values = new ArrayList<String>();
    private List<String> values2 = new ArrayList<String>();

    private String firstRun = "";
    private String secondRun = "";
    private String finalRun = "";

    private String screenshotPath = "";
    private String screenshotURL = "";
    public static String exception = "";

    private String pSuiteName = null;
    private int count = 0;
    String platform;

    public Boolean security = false;

    private int countLaunchTimes = 0;


    public ParallelTest(){
        report = new ExtentReport();
        database = new Database();
    }

    AppiumServer appiumMan = new AppiumServer();

    public void setDriver(AppiumDriver<MobileElement> driver){
        this.driver = driver;
    }

    public AppiumDriver<MobileElement> getDriver(){
        return this.driver;
    }

    public void setHelper(MyHelper helper){
        this.helper = helper;
    }

    public MyHelper getHelper(){
        return this.helper;
    }
    
    public ExtentReport getExtentReport() {
    	return report;
    }


    protected void startAppiumServer(String platform){
        this.platform = platform;
        if (platform.equals("Android")) {
            appiumMan.appiumAndroidServer();
        } else if (platform.equals("iOS")) {
            //String webKitPort = IOSDeviceConfiguration.getIosDevice().startIOSWebKit(deviceUDID);
            appiumMan.appiumServerIOS();
        }
    }

    protected void killAppiumServer(){
        appiumMan.destroyAppiumNode();
    }

    protected void launchDriver(){
        if (countLaunchTimes >0){
            driver.launchApp();
        }
    }

    protected void closeDriver(){
        driver.closeApp();
    }


    private DesiredCapabilities androidNative() {
        MyLogger.info("Setting up capabilities for Android native device");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setCapability("automationName", "uiautomator2");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MyConstants.ANDROID_DEVICE_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MyConstants.ANDROID_PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, MyConstants.ANDROID_PLATFORM_VERSION);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, MyConstants.appPackage);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, MyConstants.appActivity);
        capabilities.setCapability(AndroidMobileCapabilityType.RESET_KEYBOARD,true);
        capabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD,true);
        //capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        //capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
        return capabilities;
    }

    private DesiredCapabilities iosNative() {
        MyLogger.info("Setting up capabilities for iOS native device");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"XCUITest");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,MyConstants.IOS_PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MyConstants.IOS_DEVICE_NAME);
        capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, MyConstants.IOS_DEVICE_BUNDLEID);
        capabilities.setCapability(MobileCapabilityType.UDID, MyConstants.IOS_DEVICE_UDID);
        capabilities.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT,appiumMan.getPort());
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability("unicodeKeyboard", true);
        capabilities.setCapability("resetKeyboard", true);
        return capabilities;
    }

    protected AppiumDriver<MobileElement> startAppiumServerInParallel(String platform) {
        AppiumDriver<MobileElement> driverToStart = null;
        if (platform.equals("Android")) {
            DesiredCapabilities capabilities = androidNative();
            MyLogger.info("Appium URL with Android Platform: "+appiumMan.getAppiumUrl());
            driverToStart = new AndroidDriver<>(appiumMan.getAppiumUrl(), capabilities);
            MyLogger.info("Android driver has started");
        }
        else if (platform.equals("iOS")) {
            DesiredCapabilities capabilities = iosNative();
            MyLogger.info("Appium URL with IOS Platform: "+appiumMan.getAppiumUrl());
            MyLogger.info("iOS driver has started");
            try {
                driverToStart = new IOSDriver<>(appiumMan.getAppiumUrl(), capabilities);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return driverToStart;
    }

    public void performTestCaseUsingExcel(String sheetName, int rowNum, String fileProperties){
        String fileAction;
        String fileValidation;
        if(platform.equals("Android")){
            fileAction = "android/TestAction.xlsx";
            fileValidation = "android/TestValidation.xlsx";
            action = new AndroidUIOperation(getHelper());
        }else{
            fileAction = "ios/TestAction.xlsx";
            fileValidation = "ios/TestValidation.xlsx";
            action = new IOSUIOperation(getHelper());
        }

        XLSWorker.setSheetName(sheetName);
        if(this.workbook==null){
            this.workbook = XLSWorker.getWorkbook(MyConstants.INPUT_PATH + fileValidation);
        }

        this.fileValidation = fileValidation;
        this.sheetName = sheetName;
        this.rowNum = rowNum;

        Object[][] objects = XLSWorker.getDataForTestCase(fileAction, fileValidation, sheetName,rowNum);
        if(objects==null)
            return;
        for(int i=0; i< objects.length; i++){
            String keyword = objects[i][1].toString();
            String object = objects[i][2].toString();
            String value = objects[i][3].toString();
            MyLogger.info(keyword + " - " + object + " - " + value);
            action.perform(fileProperties,keyword,object,value);
        }
        //XLSWorker.updateExcel(XLSWorker.getSheet(workbook,sheetName),rowNum,"PASSED");
    }


    public void closeExcel(){
        XLSWorker.writeExcel(workbook);
    }

    public void setUpVariables(){     
        
    }

    public void setUpReport(ITestContext iSuite){
        Date date = new Date();
        String suiteName = iSuite.getCurrentXmlTest().getSuite().getName();
        pSuiteName = suiteName;
        if(suiteName.equals("Chrome") || suiteName.equals("Firefox") || suiteName.equals("UAT")) {
        	security = true;
        }
        String browser = StringUtils.capitalize(iSuite.getCurrentXmlTest().getParameter("browser"));
        reportPath = MyConstants.HTMLREPORT_PATH + suiteName + " - " + browser + " - "
                + FormatUtils.formatDate(date, "dd-MM-yyyy HH.mm.ss") + ".html";
        reportUrl = MyConstants.HTMLREPORT_URL + suiteName + " - " + browser + " - "
                + FormatUtils.formatDate(date, "dd-MM-yyyy HH.mm.ss") + ".html";
        report.initExtent(reportPath);
        
    }

    public void setUpDatabase(){
        database.getConnection();
        database.useDatabase(MyConstants.DB_DATABASENAME);
    }

    public void setUpTestCase(Method methodName){
        report.startExtent(methodName.getName());
        method = methodName;
    }

    public void updateExcelFinalResult(){
    	System.out.println("Update excel final result for " + sheetName);
    	if(testResult.equals("FAILED")) {
    		testResult = "FAILED: " + exception;
    	}
    	if(security == true) {
    		this.workbook1 = XLSWorker.getWorkbook(MyConstants.EXCEL_OUTPUT_PATH + "result_TestValidation_" + sheetName + ".xlsx");
    		if(this.workbook1 == null) {
    			this.workbook1 = XLSWorker.getWorkbook(MyConstants.INPUT_PATH + fileValidation);
    		}
    		XLSWorker.updateExcel(XLSWorker.getSheet(workbook1,sheetName),rowNum,testResult,screenshotURL);
            XLSWorker.writeExcel(workbook1);
    	}else {
    		XLSWorker.updateExcel(XLSWorker.getSheet(workbook,sheetName),rowNum,testResult,screenshotURL);
            XLSWorker.writeExcel(workbook);
    	}
    }
    
    

    public void updateTestResult(ITestResult iResult){
    	System.out.println("Update test result...");
        if (iResult.getStatus() == ITestResult.FAILURE || iResult.getStatus() == ITestResult.SKIP) {
            testResult = "FAILED";
            screenshotPath = getHelper().myTools().takeScreenshot(method.getName());
            screenshotURL = getHelper().myTools().getImageUrl();
        } else if (iResult.getStatus() == ITestResult.SUCCESS) {
            testResult = "PASSED";
        }
        MyLogger.info("Test result is: " + testResult);
    }

    public void insertIntoReport(){
        //report.updateExtent(testResult, method.getName(), exception, screenshotPath);
    	System.out.println("Insert result into report...");
        report.updateExtent(testResult, method.getName(), exception, screenshotURL);
        report.endExtent();
    }
    
    public void insertIntoDatabase(ITestResult iResult, ITestContext iContext){
        long duration = iResult.getEndMillis() - iResult.getStartMillis();
        long second = (duration / 1000) % 60;
        long minute = (duration / (1000 * 60)) % 60;
        long hour = (duration / (1000 * 60 * 60)) % 24;
        String durationStr = String.format("%02d:%02d:%02d", hour, minute, second, duration);
        String suiteName = iContext.getCurrentXmlTest().getSuite().getName();

        executionDate = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset,
                "yyyy-MM-dd");
        executionTime = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset,
                "yyyy-MM-dd HH:mm:ss");
        id = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset, "ddMMyyHHmmssSSS");

        if(firstRun.equals("")) {
        	firstRun = testResult;
        }
            
        if(firstRun.equals("PASSED")){
            secondRun = "PASSED";
            finalRun = secondRun;
            count = 2;
        }else{
        	count++;
        	if(count==2) {
        		secondRun = testResult;
                finalRun = secondRun;
                if(finalRun.equals("PASSED")) {
                	screenshotURL = "";
                }
        	}
        }

        if(count==2) {
        	values.add(0, id);
            values.add(1, suiteName);
            values.add(2, method.getName());
            values.add(3, executionDate);
            values.add(4, executionTime);
            values.add(5, durationStr);
            values.add(6, firstRun);
            values.add(7, secondRun);
            values.add(8, finalRun);
            values.add(9, reportUrl);
            values.add(10, screenshotURL);
            values.add(11, MyConstants.ENVIRONMENT);
            MyLogger.info("Run_ID: " + id);
            MyLogger.info("TestSuite: " + iContext.getCurrentXmlTest().getSuite().getName());
            MyLogger.info("TestMethod: " + method.getName());
            MyLogger.info("Execution Date: " + executionDate);
            MyLogger.info("Execution Time: " + executionTime);
            MyLogger.info("Duration: " + durationStr);
            MyLogger.info("First Run: " + firstRun);
            MyLogger.info("Second Run: " + secondRun);
            MyLogger.info("Final Run: " + finalRun);
            MyLogger.info("Report URL: " + reportUrl);
            MyLogger.info("Screenshot URL: " + screenshotURL);
            MyLogger.info("Environment: " + MyConstants.ENVIRONMENT);
            
            MyLogger.info("Insert result into database...");
            database.insertIntoDatabase(MyConstants.DB_TABLENAME, columns, values);
            updateExcelFinalResult();
            sendFailedResultToSkype(suiteName);
            count = 0;
            firstRun = "";
            secondRun = "";
            finalRun = "";
            screenshotURL = "";
        }
        
    }

    public void closeReport(){
        report.closeExtent();
    }

    public void closeDatabase(){
    	MyLogger.info("Closing database...");
        database.closeConnection();
    }

    public void insertIntoHeaderFile(ITestContext testSuite){
    	MyLogger.info("Updating results in header file...");
        int noOfPass = testSuite.getPassedTests().size();
        int noOfFail = testSuite.getFailedTests().size();

        XSSFWorkbook workbook = XLSWorker.getWorkbook(MyConstants.EXCEL_OUTPUT_PATH + "0Header.xlsx");
        XSSFSheet sheet = XLSWorker.getSheet(workbook,"Header");
        String testSuiteName = testSuite.getCurrentXmlTest().getSuite().getName();
     
        int row = -1;
        switch (testSuiteName){
            case "Login":
                row = 1;
                break;
            case "Chat":
                row = 2;
                break;
            case "AddNewProduct":
            	row = 3;
            	break;
            case "EditProduct":
            	row = 4;
            	break;
            case "Instock":
            	row = 5;
            	break;
            case "Promotion":
            	row = 7;
            	break;
            case "ShippingSupport":
            	row = 8;
            	break;
            case "PrivateOffer":
            	row = 9;
            	break;
            case "ShopInfo":
            	row = 10;
            	break;
            case "CancelOrder":
            	row = 11;
            	break;
            case "CarrierSuite":
            	row = 12;
            	break;
            case "Blog":
            	row = 13;
            	break;
            case "Chrome":
            	row = 14;
            	break;
            case "Firefox":
            	row = 15;
            	break;
            case "UAT":
            	row = 16;
            	break;
        }
        System.out.println("Row: " + row);
        XLSWorker.updateHeaderResult(sheet,row,noOfPass,noOfFail);
        XLSWorker.writeHeaderFile(workbook);
    }

    public void insertIntoJSONFile(ITestContext testSuite){
    	MyLogger.info("Write test result into Json file...");
    	executionTimeSuite = FormatUtils.getCurrentTimeByTimezoneOffset(
                MyConstants.timezoneOffset, "yyyy-MM-dd HH:mm:ss");
        JSONObject result = new JSONObject();
        result.put("Passed", testSuite.getPassedTests().size());
        result.put("Failed", testSuite.getFailedTests().size());
        // result.put("Pending", testSuite.getSkippedTests().size());
        result.put("ExecutionTime", executionTimeSuite.substring(5, executionTimeSuite.length()));

        JSONArray resultList = new JSONArray();
        resultList.put(result);

        JSONObject suite = new JSONObject();
        suite.put(testSuite.getCurrentXmlTest().getSuite().getName(), resultList);
        getHelper().myTools().writeJSONFile(suite, "resultSeller.json");
    }

    public void sendFailedResultToSkype(String suiteName){
        if(testResult.contains("FAILED")){
        	MyLogger.info("Send failed result via Skype...");
            Message skypeMessage = Message.create()
                    .with(Text.rich("====SELLER - Automation - "))
                    .with(Text.rich("\n TestSuite: " + suiteName).withBold())
                    .with(Text.rich(" - "))
                    .with(Text.rich("WARNING REPORT").withColor(Color.RED).withBold())
                    .with(Text.rich("\n Date: " + executionTime))
                    .with(Text.rich("\n TestCase: " + method.getName()).withBold())
                    .with(Text.rich("\n Result: " + testResult))
            		.with(Text.rich("\n Screenshot: " + screenshotURL));
            if(suiteName.equals("UAT")) {
                SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
            }else {
                SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
            }
        }
    }

    public void insertIntoTemporary(ITestContext iContext){
    	//values2.add(0, id);
        values2.add(0, "SELLER-PRO");
        values2.add(1, iContext.getCurrentXmlTest().getSuite().getName());
        values2.add(2, "");
        values2.add(3, "Running");
        values2.add(4, "0000-00-00");
        values2.add(5, "0000-00-00 00:00:00");
        values2.add(6, "00:00:00");
        values2.add(7, "");
        values2.add(8, "");
        values2.add(9, "");
        values2.add(10, "");
        values2.add(11, "");

        database.insertIntoTemporary("temporary_seller", columns2, values2);
    }
    
    public void updateStatusWithSuiteName(ITestContext iContext, String status) {
    	String suiteName = iContext.getCurrentXmlTest().getSuite().getName();
    	database.updateDatabase2("temporary_seller", "Running_status", status, suiteName);
    }
    
    public void updateResultWithSuiteName(ITestContext iContext) {
    	String suiteName = iContext.getCurrentXmlTest().getSuite().getName();
    	database.updateDatabase2("temporary_seller", "Final_result", testResult, suiteName);
    }


}
