package operation;

import constant.MyConstants;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.appium.java_client.service.local.flags.IOSServerFlag;
import logger.MyLogger;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;

public class AppiumServer {
    private AppiumDriverLocalService appiumDriverLocalService;

    private int seledroidPort;

    private void initSeledroidPort() {
        if (seledroidPort == 0)
            seledroidPort = getPort();
    }

    public int getSeledroidPort() {
        return seledroidPort;
    }

    public synchronized int getPort() {
        int port = 0;
        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            port = socket.getLocalPort();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return port;
    }

    public synchronized void appiumAndroidServer() {
        MyLogger.info("Starting Android Appium Server");
        //MyLogger.info("Device UDID: " + deviceID);
        int port = getPort();
        int chromePort = getPort();
        int bootstrapPort = getPort();
        initSeledroidPort();
        int selendroidPort = getSeledroidPort();
        MyLogger.info("BootStrapPort" + bootstrapPort);
        AppiumServiceBuilder builder = new AppiumServiceBuilder()
                .withAppiumJS(new File(MyConstants.getAppiumJSPath()))
                .withArgument(GeneralServerFlag.LOG_LEVEL, MyConstants.LOG_LEVEL)
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withIPAddress(MyConstants.IP_ADRESS)
                //.withLogFile(new File( MyConstants.APPIUM_LOGS_FOLDER +"[Android]_"+ deviceID + "__" + methodName + MyConstants.TXT_EXTENSION))
                .withArgument(AndroidServerFlag.CHROME_DRIVER_PORT, Integer.toString(chromePort))
                .withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER, Integer.toString(bootstrapPort))
                .withArgument(AndroidServerFlag.SUPPRESS_ADB_KILL_SERVER)
                .withArgument(AndroidServerFlag.SELENDROID_PORT, Integer.toString(selendroidPort)).usingPort(port)
                .usingDriverExecutable(new File(MyConstants.getNodeJsPath()));
        this.appiumDriverLocalService = AppiumDriverLocalService.buildService(builder);
        this.appiumDriverLocalService.start();
        if (!this.appiumDriverLocalService.isRunning()) {
            MyLogger.error("Appium Android service can NOT started, please check it and try again!!!!");
        }
        MyLogger.info("Is Appium Driver Local Service is running: " + this.appiumDriverLocalService.isRunning());
        MyLogger.info("Appium service builder instance is:" + builder);
    }

    public synchronized void appiumServerIOS() {
        MyLogger.info("Starting Appium Server for iOS");
        //MyLogger.info("Device UDID: " + deviceID);
        int port = getPort();
        AppiumServiceBuilder builder = new AppiumServiceBuilder()
                .withAppiumJS(new File(MyConstants.getAppiumJSPath()))
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withArgument(GeneralServerFlag.LOG_LEVEL, MyConstants.LOG_LEVEL)
                //.withLogFile(new File(MyConstants.APPIUM_LOGS_FOLDER +"[iOS]_" + deviceID + "__" + methodName + MyConstants.TXT_EXTENSION))
                //.withArgument(IOSServerFlag.WEBKIT_DEBUG_PROXY_PORT, webKitPort)
                .withIPAddress(MyConstants.IP_ADRESS)
                .usingPort(port)
                .usingDriverExecutable(new File(MyConstants.getNodeJsPath()));
        this.appiumDriverLocalService = AppiumDriverLocalService.buildService(builder);
        this.appiumDriverLocalService.start();
        if (!this.appiumDriverLocalService.isRunning()) {
            MyLogger.error("Appium iOS service can NOT started, please check it and try again!!!!");
        }
        MyLogger.info("Is Appium Driver Local Service is running: " + this.appiumDriverLocalService.isRunning());
        MyLogger.info("Appium service builder instance is: " + builder);

    }

    public URL getAppiumUrl() {
        return this.appiumDriverLocalService.getUrl();
    }

    public void destroyAppiumNode() {
        MyLogger.info("******* Stop appium local service **********");
        this.appiumDriverLocalService.stop();
        if (this.appiumDriverLocalService.isRunning()) {
            MyLogger.info("AppiumServer didn't shut... Trying to quit again....");
            this.appiumDriverLocalService.stop();
        }
    }
}