package operation;

import helpers.MyHelper;
import org.openqa.selenium.By;
import utils.ConfigReader;
import utils.FormatUtils;

public class UIOperation{

    protected MyHelper helper;

    public UIOperation(){

    }

    public UIOperation(MyHelper helper){
        this.helper = helper;
    }

    public void perform(String fileProperties, String keyword, String object, String value){
        By selector = null;
        if(!object.equals("")){
            if(!object.contains("ecom_shipping") && !object.contains("BlogContent"))
                selector = getObject(fileProperties,object);
        }

        switch (value){
            case "SKURANDOM":
                value = "SKU" + FormatUtils.getCurrentDateTime();
                break;
            case "EVENTRANDOM":
                value = "EVENT" + FormatUtils.getCurrentDateTime();
                break;
            case "CHATRANDOM":
                value = "CHAT" + FormatUtils.getCurrentDateTime();
                break;
            case "BLOGRANDOM":
                value = "BLOG" + FormatUtils.getCurrentDateTime();
                break;
            case "GETDATE":
            	value = FormatUtils.getCurrentDate();
            	break;
        }

        helper.myObjects().setProductObject(object,value);
        helper.myObjects().setShopInfoObject(object,value);
        helper.myObjects().setPromotionObject(object,value);
        helper.myObjects().setChatObject(object,value);
        helper.myObjects().setShippingSupportObject(object,value);
        helper.myObjects().setPrivateOfferObject(object,value);
        helper.myObjects().setCarrierObject(keyword, object,value);
        helper.myObjects().setBlogObject(object,value);
        helper.myObjects().setBannerObject(object,value);

        switch(keyword){
            case "SELECT":
                helper.myBasics().click(selector);
                break;
            case "SETTEXT":
                if(!value.equals("NO")) {
                	helper.myBasics().sendKeys(selector,value);
                }    
                break;
            case "PRESSNUMBER":
                helper.myBasics().pressNumber(selector, value);
                break;
            case "VERIFYTEXT":
                helper.myVerifys().verifyText(selector,value);
                break;
            case "VERIFYCHATONBUYER":
                helper.chat().verifyChatOnBuyer();
                break;
            case "VERIFYPRODUCTINFOONBUYER":
                helper.product().verifyProductFullInfoOnBuyer();
                break;
            case "VERIFYSHOPINFOONBUYER":
                helper.profile().verifyShopInfoOnBuyer();
                break;
            case "CREATEORDER":
                helper.salesOrder().createOrder();
                break;
            case "GETTEXTELEMENT":
                String valueOfElement = helper.myBasics().getElement(selector).getText();
                switch(object){
                    case "pg_ProductDetail_ProductName":
                    case "pg_ProductDetail_ProductSKU":
                    case "pg_ProductDetail_ProductPrice":
                    case "pg_ShopInfo_Phone":
                    case "pg_ShopInfo_Email":
                    case "pg_PrivateOfferDetail_ExpireDate":
                        valueOfElement = helper.myBasics().getTextFromInput(selector);
                        break;
                }
                helper.myObjects().setProductObject(object,valueOfElement);
                helper.myObjects().setShopInfoObject(object,valueOfElement);
                helper.myObjects().setChatObject(object,valueOfElement);
                helper.myObjects().setSalesOrderObject(object, valueOfElement);
                break;
        }

    }
    
    public By getObject(String fileName, String object)
    {
        String values = ConfigReader.fileRead(fileName,object);
        String[] spl = values.split(";",2);

        //find by xpath
        if(spl[1].equalsIgnoreCase("XPATH"))
        {
            return By.xpath(spl[0]);
        }

        //find by class
        else if(spl[1].equalsIgnoreCase("CLASSNAME"))
        {
            System.out.println("CLASSNAME: " + object);
            return By.className(spl[0]);
        }

        //find by id
        else if(spl[1].equalsIgnoreCase("ID"))
        {
            return By.id(spl[0]);
        }

        //find by name
        else if(spl[1].equalsIgnoreCase("NAME"))
        {
            return By.name(spl[0]);
        }

        //find by css
        else if(spl[1].equalsIgnoreCase("CSS"))
        {
            return By.cssSelector(spl[0]);
        }

        //find by link
        else if(spl[1].equalsIgnoreCase("LINKTEXT"))
        {
            return By.linkText(spl[0]);
        }

        //find by partial link
        else if(spl[1].equalsIgnoreCase("PARTIALLINK"))
        {
            return By.partialLinkText(spl[0]);
        }
        else
        {
            return null;
        }
    }
}
