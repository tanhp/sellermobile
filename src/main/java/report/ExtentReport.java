package report;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import constant.MyConstants;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ExtentReport {
    private ExtentReports extent;
    private ExtentTest test;


    private ExtentTest parent, parent1, parent2, child;


    public void initExtent(String htmlFile){
        extent = new ExtentReports(htmlFile, true);
        extent.loadConfig(new File(MyConstants.CONFIG_PATH + "extent-config.xml"));
    }

    public void startExtent(String testCase){
        test = extent.startTest(testCase);
    }

    public void endExtent(){
        extent.endTest(test);
    }

    public void updateExtent(String result, String testCase, String exception, String screenShotPath) {
        if (result.equals("FAILED")) {
            test.log(LogStatus.FAIL, testCase
                    + " <span class='test-status label right outline capitalize fail' style='align:left'>FAILED</span>");
            test.log(LogStatus.FAIL, exception);
            if(screenShotPath!=null)
            	test.log(LogStatus.FAIL, "Screenshot: " + test.addScreenCapture(screenShotPath));
        } else if (result.equals("PASSED")) {
            test.log(LogStatus.PASS, testCase
                    + " <span class='test-status label right outline capitalize pass' style='align:left'>PASSED</span>");
        } else if (result.equals("SKIPED")) {
            test.log(LogStatus.SKIP, testCase
                    + " <span class='test-status label right outline capitalize skip' style='align:left'>SKIP</span>");
        }
    }


    public void closeExtent(){
        extent.flush();
        extent.close();
    }

    @Test
    public void example(){
        extent = new ExtentReports(MyConstants.HTMLREPORT_PATH + "test.html");
        extent.loadConfig(new File(MyConstants.CONFIG_PATH + "extent-config.xml"));
        Map<String, String> sysInfo = new HashMap<>();
        sysInfo.put("Author","Tan Ho");
        sysInfo.put("Department", "SPC");
        sysInfo.put("Selenium Java Version", "3.6.0");
        sysInfo.put("AppiumVersion", "1.7.1");
        sysInfo.put("Environment", "Java");
        sysInfo.put("ProductName", "SellerUIAutomation");
        extent.addSystemInfo(sysInfo);
        parent = extent.startTest("Parent test","This is the parent test");
        parent1 = extent.startTest("Parent test 1", "This is the parent test 1").assignCategory("Category parent 1");
        parent2 = extent.startTest("Parent test 2", "This is the parent test 2").assignCategory("Category parent 2");
        child = extent.startTest("Child test","This is the child test").assignCategory("Category child");
        parent1.log(LogStatus.FAIL,"This is the testcase fail of parent 1");
        parent1.log(LogStatus.PASS, "This is the testcase pass of parent 1");
        parent2.log(LogStatus.FAIL,"This is the testcase fail of parent 2");
        parent2.log(LogStatus.PASS, "This is the testcase pass of parent 2");
        child.log(LogStatus.FAIL,"This is the testcase fail of child");
        child.log(LogStatus.PASS, "This is the testcase pass of child");
        parent1.appendChild(child);
        parent2.appendChild(child);
        extent.endTest(child);
        extent.endTest(parent2);
        extent.endTest(parent1);
        extent.endTest(parent);
        extent.flush();
        extent.close();
    }
}
