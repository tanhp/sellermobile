package api;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.zip.GZIPInputStream;

public class APIController {

    //Liti   
    static String token = "bearer cmYVmtu0JUxWVLDcBkRMPed5WZICdkYgV5Gwgf0WIFoQNxG970DYxvmiGdmL0wvk0tlRKkPH0taMBJeaq937GWGQXJQMTUqDfaTkbfuDPFC2j5juiyQN6-9sL3fdM8ilJ6PawKVxR9M1krPpF3P714FUgxqI2dh7SKi-T6leXQyF0hq9n0gDdDCS_hAS-7COhJh-Eiwhq8ug-kwVoh_Fc3AIzbWzFuypnzkUMdHKd4DI0tzduUdYQL_dxsla78Rbi567ucCW0-4YbxTMWGCPbumJ_kU_qm2RfeeVaC_8ylSwbN4wdKQT6JmQ1fJX0LoPViEuDyipQACNtVprHr9oaW_u-WlLTVLoKczy07IBBFaN8jwNWJmmku8abp-FnRX5PLw3Lrti3J-0SP4xij9NDzFW8JWGZxOWOIL2tdvnT1wnr-ogZPpWPIZyn5myZ03do_FQVNdZjAOHCUx0NH0wFNStyoGjpohsIAV8AxIcRRwcAqwL9ibyCbNI-7VFzVZZW9ejz-0ddtTFq-CUX2vVeCbNB8UDANeoH0T0we_qh-jiSkArFog0sXiDG-HmY3_MUJJIYiPsUeGdEs5YqeOI9w";
	
	//Tan Pro
    //static String token = "bearer yePdqGaPcZ3BdojU0Z61VLeG4GG7zQEEky41A8L7UkpzW2o9NWW75KOwaS-xmOoyqPU8NYnUX1BQ5cvAFMr4W4aTr191KxQnv7o9PKArGBiovABKqrc--4Y4u0yDKbEVWp8g_o08rHvrBAS3UjOgHZ1VebjZWt759LvgNEtAE3IHmC7zoomzSHBa4kZPO2YSEKcw01BZ54HFX6-sEtpvESBj39Ztkwwfbyr7zTaYdkYDC1TNIyyUOJVAZ4MsqHRVS1Gtchn8Rqi0cCsxA7siG9JQ-iYMFEWlwgF2Pkp7xFGSlRtqmrM4YU_myRBGBT9CGULpAnCyik6eFnaCgUXSygtjPokHh4ikNu7O9zECILx-m9hHfdWBkju7SdfqjiKQEMIFAuTNemfjO37Ea3MIxY8av1oVZsBVgNFhpKgDkNZ_HTPaOMOV0aNgJBRUli6T8CrkviNVCVFdT_l9jUazCi4_zfg5FwSL4-TfquWWq2fl5C3YbtQzVY-aTsxj0w8xfc_ZMbsS6hSlO7B4zoU2-oEDXoM35ZsheFpwCGjfXCBW1lGF5rzOh95xR9yuON4K";
    public static String get(String source) throws IOException {

        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;

        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Accept-Encoding", "gzip");
            conn.setRequestProperty("Authorization", token);

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Reader reader = null;
                if ("gzip".equals(conn.getContentEncoding())) {
                    reader = new InputStreamReader(new GZIPInputStream(conn.getInputStream()));
                } else {
                    reader = new InputStreamReader(conn.getInputStream());
                }
                bufReader = new BufferedReader(reader);
                response = bufReader.readLine();
                bufReader.close();
                conn.disconnect();
            } else {
                Integer responseCode = conn.getResponseCode();
                response = "error_" + responseCode.toString();
            }
        } catch (IOException ioe) {
            // log
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
            FileWriter logFile = new FileWriter("log\\api_runtime_error.txt", true);
            BufferedWriter bufferWriter = new BufferedWriter(logFile);
            PrintWriter logger = new PrintWriter(bufferWriter);
            logger.println("=======");
            logger.println(LocalDateTime.now());
            logger.println("-------");
            logger.println(source);
            logger.println("-------");
            ioe.printStackTrace(logger);
            logger.close();

        } finally {
            if (bufReader != null) {
                bufReader.close();
            }
            conn.disconnect();
        }
        return response;
    }

    public static String post(String source, String params) throws IOException {

        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;

        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Authorization", token);
            conn.setDoOutput(true);
            DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
            outStream.write(params.getBytes());
            outStream.flush();
            outStream.close();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                response = bufReader.readLine();
                bufReader.close();
                conn.disconnect();
            } else {
                Integer responseCode = conn.getResponseCode();
                response = "error_" + responseCode.toString();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            if (bufReader != null) {
                bufReader.close();
            }
            conn.disconnect();
        }
        return response;
    }

    public static String getWithAuth(String source, String authorizationCode) throws IOException {

        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;

        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Authorization", authorizationCode);

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                response = bufReader.readLine();
                bufReader.close();
                conn.disconnect();
            } else {
                Integer responseCode = conn.getResponseCode();
                response = "error_" + responseCode.toString();
            }
        } catch (IOException ioe) {
            // log
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
            FileWriter logFile = new FileWriter("log\\api_runtime_error.txt", true);
            BufferedWriter bufferWriter = new BufferedWriter(logFile);
            PrintWriter logger = new PrintWriter(bufferWriter);
            logger.println("=======");
            logger.println(LocalDateTime.now());
            logger.println("-------");
            logger.println(source);
            logger.println("-------");
            ioe.printStackTrace(logger);
            logger.close();

        } finally {
            if (bufReader != null) {
                bufReader.close();
            }
            conn.disconnect();
        }
        return response;
    }
}